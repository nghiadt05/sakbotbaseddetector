#pragma once

typedef struct {
	unsigned short header;
	union	{
		struct {
			short wvalue;
			short lvalue;
		};
		int		dw;
		float	fv;
	};
} ACMessage;

