
#include "autocar.h"

unsigned int bmPWM;
bool isSpeedIncrease;

struct ServoThreadParams {
	bool	isRunning;
	int		delay_val;
	std::mutex	mutex;
	pthread_t	id;
} sparams;

void * servoThread(void * params);

class ___sys_auto_car_ {
public:
	___sys_auto_car_() {
		// init wiringPi
		wiringPiSetupSys();
		wiringPiSetup();

		// setting up for servo
		pinMode(SERVO_PIN, OUTPUT);
		digitalWrite(SERVO_PIN, 0);
		sparams.isRunning = true;
		sparams.delay_val = 1700;
		if (-1 == pthread_create(&sparams.id, 0, servoThread, (void*)&sparams)) {
			perror("Servo Thread");
			exit(-1);
		}
		
		// setting up for motor
		bmPWM = 0;
		isSpeedIncrease = true;
		
		pinMode(MOTOR_PWM, PWM_OUTPUT);
		pwmSetMode(PWM_MODE_MS);
		pwmSetClock(3);
		pwmSetRange(1024);
		pwmWrite(MOTOR_PWM, 0);

		pinMode(MOTOR_DIR, OUTPUT);
		digitalWrite(MOTOR_DIR, 0);
	}

	~___sys_auto_car_() {
		sparams.mutex.lock();
		sparams.isRunning = false;
		sparams.mutex.unlock();
		pthread_join(sparams.id, 0);
		digitalWrite(MOTOR_DIR, 0);
	}
} ___auto_car__;

void * servoThread(void * params) {
	ServoThreadParams * p = (ServoThreadParams*)params;
	bool isRunning = p->isRunning;
	int delay = 0;
	while (isRunning) {
		p->mutex.lock();
		delay = p->delay_val;
		isRunning = p->isRunning;
		p->mutex.unlock();

		digitalWrite(SERVO_PIN, 1);
		usleep(delay);
		digitalWrite(SERVO_PIN, 0);
		usleep(20000 - delay);
	}
	return nullptr;
}

void servo_set(int angle) {
	//if (angle < -45) angle = -45;
	//if (angle > 45) angle = 45;
	sparams.mutex.lock();
	sparams.delay_val = angle * 50 / 9 + 1700; // angle * 500 / 90 + 1700
	sparams.mutex.unlock();
}

void ctrlAutoCar(int speed, int angle){
	static int last_speed = 0;
	static int last_angle = 0;
	static int last_dir = 0;
	if (speed != last_speed) {
		last_speed = speed;
		if (speed > 0) {
			if (last_dir) {
				pwmWrite(MOTOR_PWM, 0);
				usleep(100000);
			}
			last_dir = 0;
			digitalWrite(MOTOR_DIR, 0);
		}
		else {
			if (!last_dir) {
				pwmWrite(MOTOR_PWM, 0);
				usleep(100000);
			}
			last_dir = 1;
			digitalWrite(MOTOR_DIR, 1);
			speed = -speed;
		}
		pwmWrite(MOTOR_PWM, speed % 1024);
	}
	if (angle != last_angle) {
		last_angle = angle;
		servo_set(angle);
	}
}

void carStart(){	
	if(isSpeedIncrease)	{
		bmPWM +=2;
		if(bmPWM>=BM_START_PWM){
			isSpeedIncrease = false;
			bmPWM = BM_START_PWM;			
		}			
		pwmWrite(MOTOR_PWM, bmPWM % 1024);
		delayMicroseconds (50);
	}
	else{
		bmPWM -=4;		
		if(bmPWM<=BM_NORMAL_PWM){			
			bmPWM = BM_NORMAL_PWM;
		}
		else{
			delayMicroseconds (50);
		}
		pwmWrite(MOTOR_PWM, bmPWM % 1024);
	}
}

void carStop(){
	bmPWM = 0;
	isSpeedIncrease = true;
	pwmWrite(MOTOR_PWM, bmPWM);
	servo_set(0);
}




