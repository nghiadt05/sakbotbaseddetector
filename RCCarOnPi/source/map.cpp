#include "map.h"

RouteMap::RouteMap(){
	// define all route information (it is a painfull process :)) 
#if CAR_0 
	Point2d tmp;
	// R0_PATH_0 (line)
	mEntrInfo[R0_PATH_0].minX = 300;
	mEntrInfo[R0_PATH_0].maxX = 350;
	mEntrInfo[R0_PATH_0].minY = 360;
	mEntrInfo[R0_PATH_0].maxY = 405;

	mTargetLine[R0_PATH_0].type = rt_line;
	mTargetLine[R0_PATH_0].line.isUsed = true;
	mTargetLine[R0_PATH_0].expo.isUsed = false;
	mTargetLine[R0_PATH_0].line.a = 1.0;
	mTargetLine[R0_PATH_0].line.b = 120.750;
	mTargetLine[R0_PATH_0].line.c = -45756.750;
	mTargetLine[R0_PATH_0].line.A.x = 265;
	mTargetLine[R0_PATH_0].line.B.x = 650;
	mTargetLine[R0_PATH_0].line.PID_sign = +1;
	mTargetLine[R0_PATH_0].line.KP = 0.75;
	mTargetLine[R0_PATH_0].line.KI = 0.0;
	mTargetLine[R0_PATH_0].line.KD = 0.55;
	mTargetLine[R0_PATH_0].line.KG = 15.0;

	// R0_PATH_1 (curve)
	mEntrInfo[R0_PATH_1].minX = 600;
	mEntrInfo[R0_PATH_1].maxX = 650;
	mEntrInfo[R0_PATH_1].minY = 365;
	mEntrInfo[R0_PATH_1].maxY = 396;

	mTargetLine[R0_PATH_1].type = rt_expo;
	mTargetLine[R0_PATH_1].line.isUsed = false;
	mTargetLine[R0_PATH_1].expo.isUsed = true;
	mTargetLine[R0_PATH_1].expo.a = 50.733482;
	mTargetLine[R0_PATH_1].expo.b = 0.040602;
	mTargetLine[R0_PATH_1].expo.Offset = Point2d(775, 380);
	mTargetLine[R0_PATH_1].expo.xMin = 650;
	mTargetLine[R0_PATH_1].expo.xMax = 760;
	mTargetLine[R0_PATH_1].expo.PID_sign = +1;
	mTargetLine[R0_PATH_1].expo.KP = 0.75;
	mTargetLine[R0_PATH_1].expo.KI = 0;
	mTargetLine[R0_PATH_1].expo.KD = 0.55;
	mTargetLine[R0_PATH_1].expo.KG = 40.0;

	// R0_PATH_2 (curve)
	mEntrInfo[R0_PATH_2].minX = 740;
	mEntrInfo[R0_PATH_2].maxX = 840;
	mEntrInfo[R0_PATH_2].minY = 450;
	mEntrInfo[R0_PATH_2].maxY = 500;

	mTargetLine[R0_PATH_2].type = rt_expo;
	mTargetLine[R0_PATH_2].line.isUsed = false;
	mTargetLine[R0_PATH_2].expo.isUsed = true;
	mTargetLine[R0_PATH_2].expo.a = -72.149474;
	mTargetLine[R0_PATH_2].expo.b = 0.051388;
	mTargetLine[R0_PATH_2].expo.Offset = Point2d(750, 543);
	mTargetLine[R0_PATH_2].expo.xMin = 705;
	mTargetLine[R0_PATH_2].expo.xMax = 760;
	mTargetLine[R0_PATH_2].expo.PID_sign = -1;
	mTargetLine[R0_PATH_2].expo.KP = 0.75;
	mTargetLine[R0_PATH_2].expo.KI = 0;
	mTargetLine[R0_PATH_2].expo.KD = 0.55;
	mTargetLine[R0_PATH_2].expo.KG = 40;

	// R0_PATH_3 (line)
	mEntrInfo[R0_PATH_3].minX = 600;
	mEntrInfo[R0_PATH_3].maxX = 680;
	mEntrInfo[R0_PATH_3].minY = 500;
	mEntrInfo[R0_PATH_3].maxY = 560;

	mTargetLine[R0_PATH_3].type = rt_line;
	mTargetLine[R0_PATH_3].line.isUsed = true;
	mTargetLine[R0_PATH_3].expo.isUsed = false;
	mTargetLine[R0_PATH_3].line.a = 1.0;
	mTargetLine[R0_PATH_3].line.b = 154;
	mTargetLine[R0_PATH_3].line.c = -82867;
	mTargetLine[R0_PATH_3].line.A.x = 200;
	mTargetLine[R0_PATH_3].line.B.x = 705;
	mTargetLine[R0_PATH_3].line.PID_sign = -1;
	mTargetLine[R0_PATH_3].line.KP = 0.75;
	mTargetLine[R0_PATH_3].line.KI = 0;
	mTargetLine[R0_PATH_3].line.KD = 0.55;
	mTargetLine[R0_PATH_3].line.KG = 15.0;

	// R0_PATH_4 (curve)
	mEntrInfo[R0_PATH_4].minX = 250;
	mEntrInfo[R0_PATH_4].maxX = 300;
	mEntrInfo[R0_PATH_4].minY = 516;
	mEntrInfo[R0_PATH_4].maxY = 563;

	mTargetLine[R0_PATH_4].type = rt_expo;
	mTargetLine[R0_PATH_4].line.isUsed = false;
	mTargetLine[R0_PATH_4].expo.isUsed = true;
	mTargetLine[R0_PATH_4].expo.a = -53.641183;
	mTargetLine[R0_PATH_4].expo.b = -0.062196;
	mTargetLine[R0_PATH_4].expo.Offset = Point2d(160, 530);
	mTargetLine[R0_PATH_4].expo.xMin = 190;
	mTargetLine[R0_PATH_4].expo.xMax = 220;
	mTargetLine[R0_PATH_4].expo.PID_sign = -1;
	mTargetLine[R0_PATH_4].expo.KP = 0.75;
	mTargetLine[R0_PATH_4].expo.KI = 0;
	mTargetLine[R0_PATH_4].expo.KD = 0.55;
	mTargetLine[R0_PATH_4].expo.KG = 40.0;

	// R0_PATH_5 (curve)
	mEntrInfo[R0_PATH_5].minX = 134;
	mEntrInfo[R0_PATH_5].maxX = 225;
	mEntrInfo[R0_PATH_5].minY = 450;
	mEntrInfo[R0_PATH_5].maxY = 500;

	mTargetLine[R0_PATH_5].type = rt_expo;
	mTargetLine[R0_PATH_5].line.isUsed = false;
	mTargetLine[R0_PATH_5].expo.isUsed = true;
	mTargetLine[R0_PATH_5].expo.a = 50.829808;
	mTargetLine[R0_PATH_5].expo.b = -0.06029;
	mTargetLine[R0_PATH_5].expo.Offset = Point2d(205, 380);
	mTargetLine[R0_PATH_5].expo.xMin = 190;
	mTargetLine[R0_PATH_5].expo.xMax = 300;
	mTargetLine[R0_PATH_5].expo.PID_sign = +1;
	mTargetLine[R0_PATH_5].expo.KP = 0.75;
	mTargetLine[R0_PATH_5].expo.KI = 0;
	mTargetLine[R0_PATH_5].expo.KD = 0.55;
	mTargetLine[R0_PATH_5].expo.KG = 30;

	// create sample points for all target lines
	for (int j = R0_PATH_0; j <= R0_PATH_5; j++){
		for (int i = 0; i < NUM_ROUTE_SAMPLE; i++){
			if (mTargetLine[j].type == rt_expo){
				tmp.x = mTargetLine[j].expo.xMin +
					(mTargetLine[j].expo.xMax - mTargetLine[j].expo.xMin)*i / NUM_ROUTE_SAMPLE;
				tmp.y = mTargetLine[j].expo.a*exp(mTargetLine[j].expo.b*(tmp.x - mTargetLine[j].expo.Offset.x)) +
					mTargetLine[j].expo.Offset.y;
				mTargetLine[j].expo.Samples.push_back(tmp);
			}
			else if (mTargetLine[j].type == rt_line){
				mTargetLine[j].line.A.y = -(mTargetLine[j].line.A.x + mTargetLine[j].line.c) / mTargetLine[j].line.b;
				mTargetLine[j].line.B.y = -(mTargetLine[j].line.B.x + mTargetLine[j].line.c) / mTargetLine[j].line.b;
			}
		}
	}
#endif
}

RouteMap::~RouteMap(){};