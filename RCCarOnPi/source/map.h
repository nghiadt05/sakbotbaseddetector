#ifndef _MAP_H_
#define _MAP_H_

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/video/tracking.hpp>

using		namespace			cv;
using		namespace			std;

#define MAP_FILE "../MapImages/BG.jpg"

#define CAR_0						1
#define CAR_1						0

#define NUM_ROUTE_SAMPLE			200
#define NULL_PATH					254

#define R0_PATH_0					0
#define R0_PATH_1					1
#define R0_PATH_2					2
#define R0_PATH_3					3
#define R0_PATH_4					4
#define R0_PATH_5					5
#define NUM_PATH					6				

enum RouteType
{
	rt_line,
	rt_expo
};

class RouteMap{
public: // variables 
	struct EntranceInfo
	{
		double minX, maxX;
		double minY, maxY;
	};
	struct TL_Line{
		// ax + by + c = 0;
		int PID_sign;
		double E[2], I;
		double KP, KI, KD, KG;
		bool isUsed;
		double a, b, c;
		Point2d A, B;
	};

	struct TL_Expo{
		// y = a*exp(b*x)
		int PID_sign;
		double E[2], I;
		double KP, KI, KD, KG;
		bool isUsed;
		double a, b;
		Point2d Offset;
		double xMin, xMax;
		vector<Point2d>Samples;
	};

	struct TargetLine{
		int type;
		TL_Line line;
		TL_Expo expo;
	};

	TargetLine mTargetLine[NUM_PATH];
	EntranceInfo mEntrInfo[NUM_PATH];

public: // functions
	RouteMap();
	~RouteMap();
};

#endif