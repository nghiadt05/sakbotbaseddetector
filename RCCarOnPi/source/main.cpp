#include "main.h"

int main(int argv, const char * argc[]){

	//wifi setup
	s.socket = -1;
	c.socket = -1;
	lpUpdate = waitClient;
	if (openServer() == -1) return -1;
	printf("Server started!\n\r");

#if SHOW_MAP
	// display the map and the generated routes for debugging
	Mat map = imread(MAP_FILE, CV_LOAD_IMAGE_COLOR);
	assert(map.empty() == 0 && "Cannot read image");

	// entrance info
#if CAR_0 
	for (uint8_t j = R0_PATH_0; j <= R0_PATH_5; j++){
		Point2d tmp[4];
		tmp[0] = Point2d(mRouteMap.mEntrInfo[j].minX, mRouteMap.mEntrInfo[j].minY);
		tmp[1] = Point2d(mRouteMap.mEntrInfo[j].minX, mRouteMap.mEntrInfo[j].maxY);
		tmp[2] = Point2d(mRouteMap.mEntrInfo[j].maxX, mRouteMap.mEntrInfo[j].minY);
		tmp[3] = Point2d(mRouteMap.mEntrInfo[j].maxX, mRouteMap.mEntrInfo[j].maxY);
		circle(map, tmp[0], 1, Scalar(0, 0, 250), 3, 4, 0);
		circle(map, tmp[1], 1, Scalar(0, 0, 250), 3, 4, 0);
		circle(map, tmp[2], 1, Scalar(0, 0, 250), 3, 4, 0);
		circle(map, tmp[3], 1, Scalar(0, 0, 250), 3, 4, 0);
		line(map, tmp[0], tmp[1], Scalar(0, 0, 250), 2, 4, 0);
		line(map, tmp[0], tmp[2], Scalar(0, 0, 250), 2, 4, 0);
		line(map, tmp[2], tmp[3], Scalar(0, 0, 250), 2, 4, 0);
		line(map, tmp[1], tmp[3], Scalar(0, 0, 250), 2, 4, 0);
		
		if (mRouteMap.mTargetLine[j].expo.isUsed){
			for (unsigned i = 0; i < NUM_ROUTE_SAMPLE; i++){
				Point2d tmp = mRouteMap.mTargetLine[j].expo.Samples.at(i);
				circle(map, tmp, 1, Scalar(0, 255, 0), 1, 4, 0);
			}
		}
		else if (mRouteMap.mTargetLine[j].line.isUsed){
			line(map, mRouteMap.mTargetLine[j].line.A, mRouteMap.mTargetLine[j].line.B, Scalar(0, 255, 0), 2, 4, 0);
		}
	}
#endif
	imshow("Map", map);
	waitKey(0);
#endif

	while (1) {
		// Fix-loop-time operations for the PID algorithm
		last_start_time = start_time;
		start_time = millis();

		// Determine the target line based on the current position and the
		// coressponding entrance locations
		isInCrossedSec = false;
#if CAR_0 
		for (uint8_t j = R0_PATH_0; j <= R0_PATH_5; j++){
#endif
			if (CurPos.x > mRouteMap.mEntrInfo[j].minX && CurPos.x < mRouteMap.mEntrInfo[j].maxX &&
				CurPos.y > mRouteMap.mEntrInfo[j].minY && CurPos.y < mRouteMap.mEntrInfo[j].maxY){
				// change target line
				if(targetIndx != j){
					targetIndx = j;				
					isHisPosAvai = false;
					hisPosCnt = 0;				
				}
				isInCrossedSec = true;
			}
		}

		// Wait for the host PC's permission before moving.
		// The host PC will send the start/stop signals based on its algorithm.
		// In the car side, we only need to wait for the start signal, other than that this car should stop.		
		if (isStart && targetIndx!= NULL_PATH){
			// Update the history positions iff the car has moved 
			if (abs(CurPos.x - HisPos[HIS_POS_SIZE - 1].x) >= DIFF_POS_MIN || abs(CurPos.y - HisPos[HIS_POS_SIZE - 1].y) >= DIFF_POS_MIN){
				for (uint8_t i = 0; i < (HIS_POS_SIZE - 1); i++)
					HisPos[i] = HisPos[i + 1];
				
				HisPos[HIS_POS_SIZE - 1] = CurPos;

				hisPosCnt++;
				if (hisPosCnt >= HIS_POS_SIZE){
					// Estimate the future position based on the history positions
					difPos[0] = Point2i(HisPos[HIS_POS_SIZE - 1].x - HisPos[HIS_POS_SIZE - 3].x, HisPos[HIS_POS_SIZE - 1].y - HisPos[HIS_POS_SIZE - 3].y);
					difPos[1] = Point2i(HisPos[HIS_POS_SIZE - 3].x - HisPos[HIS_POS_SIZE - 5].x, HisPos[HIS_POS_SIZE - 3].y - HisPos[HIS_POS_SIZE - 5].y);
					difPos[2] = Point2i(HisPos[HIS_POS_SIZE - 5].x - HisPos[HIS_POS_SIZE - 7].x, HisPos[HIS_POS_SIZE - 5].y - HisPos[HIS_POS_SIZE - 7].y);
					difPos[3] = Point2i(HisPos[HIS_POS_SIZE - 7].x - HisPos[HIS_POS_SIZE - 9].x, HisPos[HIS_POS_SIZE - 7].y - HisPos[HIS_POS_SIZE - 9].y);
					predDiff = Point2i(0.4*difPos[0].x + 0.2*difPos[1].x + 0.2*difPos[2].x + 0.4*difPos[3].x,
						0.4*difPos[0].y + 0.2*difPos[1].y + 0.2*difPos[2].y + 0.4*difPos[3].y);
					hisPosCnt = HIS_POS_SIZE;
					isHisPosAvai = true;
				}
			}

			// Calculate the distance based on the received positions and load the necessary parameters
			if (mRouteMap.mTargetLine[targetIndx].expo.isUsed){
				// Load the PID gains if the host computer has sent them
				if (isPIDUpdate){
					mRouteMap.mTargetLine[targetIndx].expo.KP = KP;
					mRouteMap.mTargetLine[targetIndx].expo.KI = KI;
					mRouteMap.mTargetLine[targetIndx].expo.KD = KD;
					mRouteMap.mTargetLine[targetIndx].expo.KG = KG;
					printf("PID values updated\n");
					isPIDUpdate = false;
				}

				a = mRouteMap.mTargetLine[targetIndx].expo.a;
				b = mRouteMap.mTargetLine[targetIndx].expo.b;
				PID_sign = mRouteMap.mTargetLine[targetIndx].expo.PID_sign;
				offset = mRouteMap.mTargetLine[targetIndx].expo.Offset;
				PredPos = Point2i(mRouteMap.mTargetLine[targetIndx].expo.KG * predDiff.x + CurPos.x,
					mRouteMap.mTargetLine[targetIndx].expo.KG * predDiff.y + CurPos.y);
				KP = mRouteMap.mTargetLine[targetIndx].expo.KP;
				KI = mRouteMap.mTargetLine[targetIndx].expo.KI;
				KD = mRouteMap.mTargetLine[targetIndx].expo.KD;

				E[0] = CurPos.y - (float)(a*exp(b*((double)CurPos.x - offset.x)) + offset.y);
				E[1] = PredPos.y - (float)(a*exp(b*((double)PredPos.x - offset.x)) + offset.y);
			}
			else if (mRouteMap.mTargetLine[targetIndx].line.isUsed){
				// Load the PID gains if the host computer has sent them
				if (isPIDUpdate){
					mRouteMap.mTargetLine[targetIndx].line.KP = KP;
					mRouteMap.mTargetLine[targetIndx].line.KI = KI;
					mRouteMap.mTargetLine[targetIndx].line.KD = KD;
					mRouteMap.mTargetLine[targetIndx].line.KG = KG;
					printf("PID values updated\n");
					isPIDUpdate = false;
				}

				b = mRouteMap.mTargetLine[targetIndx].line.b;
				cc = mRouteMap.mTargetLine[targetIndx].line.c;
				PID_sign = mRouteMap.mTargetLine[targetIndx].line.PID_sign;
				PredPos = Point2i(mRouteMap.mTargetLine[targetIndx].line.KG * predDiff.x + CurPos.x,
					mRouteMap.mTargetLine[targetIndx].line.KG * predDiff.y + CurPos.y);
				KP = mRouteMap.mTargetLine[targetIndx].line.KP;
				KI = mRouteMap.mTargetLine[targetIndx].line.KI;
				KD = mRouteMap.mTargetLine[targetIndx].line.KD;

				E[0] = (CurPos.x + b*CurPos.y + cc) / sqrt(1 + b*b);
				E[1] = (PredPos.x + b*PredPos.y + cc) / sqrt(1 + b*b);
			}

			// Self-control using the PID algorithm
			carStart();
			
			dt = (float)(start_time - last_start_time);
			int static angle = 0, l_angle = 0;
			l_angle = angle;
			if (!isHisPosAvai){
				PID_Out[0] = KP * E[0] + KD * E[0] / dt;
				angle = PID_sign * (int)(PID_Out[0]);
			}
			else{
				PID_Out[1] = KP * E[1] + KD * E[1] / dt;
				angle = PID_sign * (int)(PID_Out[1]);
			}			
			angle = CONSTRAIN(angle, MIN_SEVOR_OUT, MAX_SEVOR_OUT);
			if(isInCrossedSec){
				angle = (angle + 3*l_angle)/4;
			}
			cout << (int)targetIndx << " " << isHisPosAvai << " " << CurPos << " " << PredPos << " " << E[0] << " " << E[1] << " " << angle << "\n\r";
			servo_set(angle);
		}

		// update wifi status
		if (lpUpdate() != 0) return -1;

		// fix the loop time
		while (millis() - start_time < FIXED_TIME)
			delayMicroseconds(10);
	}

	return 0;
}

int openServer(){
	int on = 1;

	s.socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (s.socket < 0) {
		perror("socket");
		return -1;
	}
	if (setsockopt(s.socket, SOL_SOCKET, SO_REUSEADDR, (const char*)&on, sizeof(on)) < 0) {
		closeServer();
		perror("socket");
		return -1;
	}
	memset(&s.addr, 0, sizeof(s.addr));
	s.addr.sin_family = AF_INET;
	s.addr.sin_addr.s_addr = htonl(INADDR_ANY);
	s.addr.sin_port = htons(SERVER_PORT);

	if (bind(s.socket, (struct sockaddr *)&s.addr, sizeof(s.addr)) < 0) {
		closeServer();
		perror("socket");
		return -1;
	}
	return 0;
}

int waitClient(){
	socklen_t len = sizeof(c.addr);
	wid = 0;
	rid = 0;

	listen(s.socket, 1);
	c.socket = accept(s.socket, (struct sockaddr *)&c.addr, &len);
	if (c.socket < 0) {
		perror("Client");
		closeServer();
		return -1;
	}
	if (fcntl(c.socket, F_SETFL, O_NONBLOCK) < 0) return 0;
	printf("Connected client: %s\n", inet_ntoa(c.addr.sin_addr));

	lpUpdate = updateServer;
#ifdef TIME_CONSTRAINT
	time(&last_time);
#endif
	return 0;
}

int closeServer(){
	if (s.socket >= 0) close(s.socket);
	if (c.socket >= 0) close(c.socket);
	s.socket = -1;
	c.socket = -1;
	return 0;
}

int updateServer() {
	int len;
	len = (int)recv(c.socket, buffer + wid, 512, 0);
	if (len == 0) {
		printf("Close client!\n");
		ctrlAutoCar(0, 0);
		close(c.socket);
		c.socket = -1;
		lpUpdate = waitClient;
	}
	else if (len > 0){
		ACMessage * p = (ACMessage*)buffer;
		wid += len;
		while ((wid - rid) >= sizeof(ACMessage)) {
			parseMessage(p);
			rid += sizeof(ACMessage);
		}
		if (rid != 0) {
			for (int i = rid; i < wid; i++) {
				buffer[i - rid] = buffer[i];
			}
			wid -= rid;
			rid = 0;
		}
	}
	else {
#ifdef TIME_CONSTRAINT
		time_t t;
		time(&t);
		if (difftime(t, last_time) > TIME_CONSTRAINT) {
			ctrlAutoCar(0, 0);
		}
#endif
		usleep(10000);
	}
	return 0;
}

int parseMessage(ACMessage * pmsg)
{
	//printf("parse message: %d %d %d\n", pmsg->header, pmsg->wvalue, pmsg->lvalue);
	// got the control commands and perform them
	switch (pmsg->header) {
	case AC_CONTROL:
		if (pmsg->fv == CAR_START){
			printf("Car start\n");
			isStart = (bool)CAR_START;
		}
		else if (pmsg->fv == CAR_STOP){
			printf("Car stop\n");
			isStart = (bool)CAR_STOP;
			carStop();
		}
		break;
	case AC_CURPOS:
		CurPos = Point2i(pmsg->wvalue, pmsg->lvalue);
		//cout << "CurPos = " << CurPos << "\n\r";
		break;
	case AC_KP:
		isPIDUpdate = true;
		KP = pmsg->fv;
		cout << "KP = " << KP << "\n\r";
		break;
	case AC_KD:
		isPIDUpdate = true;
		KD = pmsg->fv;
		cout << "KD = " << KD << "\n\r";
		break;
	case AC_KI:
		isPIDUpdate = true;
		KI = pmsg->fv;
		cout << "KI = " << KI << "\n\r";
		break;
	case AC_KG:
		isPIDUpdate = true;
		KG = pmsg->fv;
		cout << "KG = " << KG << "\n\r";
		break;
	}
	return 0;
}
