#ifndef _COMMON_H_
#define _COMMON_H_

// Linux and C/C++ libs 
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/time.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdarg.h>
#include <netdb.h>
#include <string.h>
#include <errno.h>
#include <queue>
#include <time.h>
#include <stdio.h>

// Pi

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/video/tracking.hpp>

using		namespace			cv;
using		namespace			std;

// marcos
#define CONSTRAIN(amt,low,high) ((amt)<(low)?(low):((amt)>(high)?(high):(amt)))

// Configurations
#define SHOW_MAP 	0

#endif