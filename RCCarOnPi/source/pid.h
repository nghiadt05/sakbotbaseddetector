#ifndef _PID_H_
#define _PID_H_

#include "common.h"

#define HIS_POS_SIZE 9
#define DIFF_POS_MIN 2

extern double E[2];
extern double I[2];
extern double KP;
extern double KD;
extern double KI;
extern double KG;
extern double PID_Out[2];
extern double dt;
extern Point2i CurPos;
extern Point2i PredPos;
extern Point2i HisPos[HIS_POS_SIZE];

#endif
