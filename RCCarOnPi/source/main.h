#include "common.h"
#include "autocar.h"
#include "ACMessage.h"
#include "pid.h"
#include "map.h"

#define SERVER_PORT		5678
#define FIXED_TIME 		10
//#define TIME_CONSTRAINT	2.0		// seconds

RouteMap mRouteMap;

typedef int(*LP_SERVER_PROC)(void);
LP_SERVER_PROC lpUpdate;

struct {
	int					socket;
	struct sockaddr_in	addr;
} s, c;

unsigned char buffer[1024];
int wid = 0;
int rid = 0;

bool isStart = false;
bool isReady2Move = false;
bool isPIDUpdate = false;

int8_t PID_sign;
uint32_t start_time;
uint32_t last_start_time;
double a, b, cc;
uint8_t hisPosCnt;
bool isHisPosAvai;
Point2d offset;
Point2i difPos[4];
Point2i predDiff;
int targetIndx = NULL_PATH;
bool isInCrossedSec = false; 

#ifdef TIME_CONSTRAINT
time_t  last_time;	// time constraint
#endif

int openServer();
int waitClient();
int updateServer();
int parseMessage(ACMessage* pmsg);
int closeServer();