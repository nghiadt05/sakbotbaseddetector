#include <stdio.h>
#include <mutex>
#include <unistd.h>
#include <pthread.h>
#include <wiringPi.h>

#pragma once

#define	AC_ACK			0
#define	AC_CONTROL		1
#define AC_CURPOS		2
#define AC_KP			3
#define AC_KI			4
#define	AC_KD			5
#define AC_KG			6

#define CAR_STOP		0
#define CAR_START		1

#define SERVO_PIN		0
#define MOTOR_PWM		1
#define MOTOR_DIR		4

// back motor
#define BM_STOP_PWM 	0
#define BM_START_PWM    130
#define BM_NORMAL_PWM   70

// servor motor
#define MIN_SEVOR_OUT -45
#define MAX_SEVOR_OUT  45

extern unsigned int bmPWM;
extern bool isSpeedIncrease;

void ctrlAutoCar(int speed, int angle);
void carStart(void);
void carStop(void);
void servo_set(int angle);