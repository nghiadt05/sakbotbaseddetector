%%
clc; close all; clear all;
%%
frame = imread('bg.jpg');
fid = fopen('entrance.info','w');
figure('name','BG');
imshow(frame);
%% entrance 1 boundary
fprintf('Chose 4-corner points of entrance 1 !\n');
[xA, yA] = ginput(4);
hold on;
plot(xA,yA,'r+','MarkerSize',20);
hold off;
fprintf('Entrance A: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xA),max(xA), min(yA), max(yA));
%% entrance 2 boundary
fprintf('Chose 4-corner points of entrance 2 !\n');
[xB, yB] = ginput(4);
hold on;
plot(xB,yB,'r+','MarkerSize',20);
hold off;
fprintf('Entrance B: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xB),max(xB), min(yB), max(yB));
%% entrance 3 boundary
fprintf('Chose 4-corner points of entrance 3 !\n');
[xC, yC] = ginput(4);
hold on;
plot(xC,yC,'r+','MarkerSize',20);
hold off;
fprintf('Entrance C: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xC),max(xC), min(yC), max(yC));
%% entrance 4 boundary
fprintf('Chose 4-corner points of entrance 4 !\n');
[xD, yD] = ginput(4);
hold on;
plot(xD,yD,'r+','MarkerSize',20);
hold off;
fprintf('Entrance D: [minX, maxX] = [%0.3f, %0.3f] [minY, maxY] = [%0.3f, %0.3f]\n',min(xD),max(xD), min(yD), max(yD));
%% print out to file
fprintf(fid,'Entrance A: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xA),max(xA), min(yA), max(yA));
fprintf(fid,'Entrance B: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xB),max(xB), min(yB), max(yB));
fprintf(fid,'Entrance C: [minX, maxX] = [%0.3f, %0.3f] [minY, max Y] = [%0.3f, %0.3f]\n',min(xC),max(xC), min(yC), max(yC));
fprintf(fid,'Entrance D: [minX, maxX] = [%0.3f, %0.3f] [minY, maxY] = [%0.3f, %0.3f]\n',min(xD),max(xD), min(yD), max(yD));
fclose(fid);






