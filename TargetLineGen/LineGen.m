clc; close all; clear all;
fprintf('Chose 2 sample points for the straight line !\n');
figure('name','BG_Straight_line');
frame = imread('bg.jpg');
imshow(frame);
[xS, yS] = ginput(2);
b = (xS(2)-xS(1))/(yS(1)-yS(2));
c = -(xS(1)+b*yS(1));
xStmp = min(xS):0.1:max(xS); xStmp = xStmp';
yStmp = -(xStmp + c)/b;

fprintf('[a b c] = [1.0 %0.3f %0.3f]\n',b,c);
fprintf('[minX maxX] = [%0.2f %0.2f]\n',min(xS), max(xS));
hold on;
plot(xStmp,yStmp,'g.');
hold off;