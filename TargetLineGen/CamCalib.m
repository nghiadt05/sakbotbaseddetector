%%
clc; close all; clear all;
%% configuration
opt.calib = false;
opt.measure = true;
%% measure the map's keypoints
if(opt.measure)
    frame = imread('bg.jpg');
    figure('name','BG');
    frame = imresize(frame,[320,480]);
    imshow(frame);
    fprintf('Pick the bottom-left and bottom-right key points of the map\n');
    [x, y] = ginput(2);
    hold on;
    plot(x,y,'r+','MarkerSize',20);
    plot(x,y,'g','LineWidth',2);
    hold off;
    KeyPoints.x = x;
    KeyPoints.y = y;
    save('KeyPoints.mat','KeyPoints');
end
%% calibration
if(opt.calib)
    load('KeyPoints.mat');
    cam = webcam('Logitech');
    figure('name','CamCalib');
    while(1)
        % read image from camera
        frame = snapshot(cam);        
        frame = imresize(frame,[320,480]);
        imshow(frame);
        hold on;        
        plot(KeyPoints.x,KeyPoints.y,'r+','MarkerSize',20); 
        plot(KeyPoints.x,KeyPoints.y,'g','LineWidth',2);
        hold off;        
    end
end