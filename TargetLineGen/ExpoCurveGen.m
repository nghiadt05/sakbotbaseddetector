function ExpoCurveGen(max_min_x, max_min_y)
    %%
    %clc; close all; clear all;
    %% read image file
    frame = imread('bg.jpg');
    %% get points for the right-turn line
    fprintf('Chose 30 sample points for a curve !\n');
    figure('name','BG curve gen');
    imshow(frame);
    [xR, yR] = ginput(30);
    if(max_min_x==1)
        xR0 = max(xR); 
    elseif(max_min_x==0)
        xR0 = min(xR); 
    end
    
    if(max_min_y==1)
        yR0 = max(yR);
    elseif(max_min_y==0)
        yR0 = min(yR);
    end     
    xRtmp = xR-xR0;
    yRtmp = yR-yR0;
    
    f = fit(xRtmp,yRtmp,'exp1');
    xRtmp = min(xRtmp):0.1:max(xRtmp); xRtmp = xRtmp';
    yRtmp = f(xRtmp);
    xRtmp = xRtmp + xR0;
    yRtmp = yRtmp + yR0;

    hold on;
    plot(xRtmp,yRtmp,'g.');
    plot(xR,yR,'r.');
    hold off;
    
    cof = coeffvalues(f);
    fprintf('General model Exp1:\nans(x) = a*exp(b*x)\n');
    fprintf('[a b] = [%0.6f %0.6f]\n', cof(1), cof(2));
    fprintf('[offsetX offsetY] = [%0.3f %0.3f]\n',xR0,yR0);
    fprintf('[minX maxX] = [%0.3f %0.3f]\n\n',min(xR),max(xR));
end



