%%
clc; close all; clear all;

%%
opts.NegImgsDir = './NegImgs';
opts.PosImgsDir = './PosImgs';
opts.FileExt    = '.jpg';
opts.ImgSizeVer = 270;
opts.ImgSizeHor = 480;
%%
% Extract features from positive and negative training images then save to
% csv files
MLNNFet   = [];
MLNNLabel = [];
TrainData = [];
for i=1:2
    if i==1
        ImgFile = dir([opts.NegImgsDir,'/*',opts.FileExt]);     
        Label = 0;
    else
        ImgFile = dir([opts.PosImgsDir,'/*',opts.FileExt]);        
        Label = 1;
    end
    
    for imgIdx = 1:numel(ImgFile)
        if i==1
            MVO = imread([opts.NegImgsDir,'/',ImgFile(imgIdx).name]);   
        else
            MVO = imread([opts.PosImgsDir,'/',ImgFile(imgIdx).name]);   
        end
        %% Symetric feature        
        if mod(size(MVO,1),2)==1
            MVO = MVO(1:end-1,:);            
        end    
        
        if mod(size(MVO,2),2)==1
            MVO = MVO(:,1:end-1);        
        end
        % verticaly symmetric features
        partVerA = MVO(1:end/2,:);
        partVerB = MVO((end/2+1):end,:);
        SymVer = partVerA - partVerB;
        SymVer = size(find(SymVer),1)/(size(MVO,1)*size(MVO,2));
        % horizontaly symmetric features        
        partHorA = MVO(:,1:end/2);
        partHorB = MVO(:,(end/2+1):end);
        SymHor = partHorA - partHorB;
        SymHor = size(find(SymHor),1)/(size(MVO,1)*size(MVO,2));
        %% size fet.   
        v       = size(MVO,1)/opts.ImgSizeVer;
        h       = size(MVO,2)/opts.ImgSizeHor;     
        %% density fet.
        density = size(find(MVO),1)/(size(MVO,1)*size(MVO,2));     
       
        %% store to train Matlab NN
        MLNNFet = [MLNNFet,[SymVer;SymHor;v;h;density]];
        TrainData = [TrainData;[SymVer,SymHor,v,h,density,Label]];
        if i==1
            MLNNLabel = [MLNNLabel,[0;1]];
        else
            MLNNLabel = [MLNNLabel,[1;0]];
        end
    end   
end

%% Shuffle the train data then create the test data
odering = randperm(size(TrainData,1));
TrainData = TrainData(odering,:);
TestDataIdx = randi(size(TrainData,1),floor([1,0.2*size(TrainData,1)]));
TestDataIdx = unique(TestDataIdx);
TestData = TrainData(TestDataIdx,:);
TrainData(TestDataIdx,:) = [];

%% Print to csv files
DataFile = './TrainData.csv';   
fDataId = fopen(DataFile,'w');  
for j=1:size(TrainData,1)
    tmpFet = TrainData(j,:);
    fprintf(fDataId,'%f,%f,%f,%f,%f,%d\n',tmpFet(1),tmpFet(2),tmpFet(3),tmpFet(4),tmpFet(5),tmpFet(6));        
end
fclose(fDataId);

DataFile = './TestData.csv';   
fDataId = fopen(DataFile,'w');  
for j=1:size(TestData,1)
    tmpFet = TestData(j,:);
    fprintf(fDataId,'%f,%f,%f,%f,%f,%d\n',tmpFet(1),tmpFet(2),tmpFet(3),tmpFet(4),tmpFet(5),tmpFet(6));        
end
fclose(fDataId);
%% Test the feature using Matlab NN
net = patternnet([5,5]);
[net,tr] = train(net,MLNNFet,MLNNLabel);
nntraintool;
plotperform(tr);
testX = MLNNFet(:,tr.testInd);
testT = MLNNLabel(:,tr.testInd);
testY = net(testX);
ll = mlogloss(testT(1,:),testY(1,:));
testT = vec2ind(testT);
testY = vec2ind(testY);
error = sum(testT~=testY)/size(testT,2);
fprintf('Based-line classifier error = %0.5f\n',error);
fprintf('Based-line log-loss error = %0.5f\n',ll);
 