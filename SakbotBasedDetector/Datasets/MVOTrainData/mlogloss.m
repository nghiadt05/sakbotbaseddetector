function ll = mlogloss( actual, predicted )
 eps = 0.01;
 predicted = min( max( predicted, eps ), 1 - eps );
 ll = -1 / numel( actual ) * ( sum(( actual .* log( predicted ) ...
  + ( 1 - actual ) .* log( 1 - predicted ))));
end