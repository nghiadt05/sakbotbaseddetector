%{
    Brief: Extract images from testing Datasetses and store them
%}
global Dataset;

%% Configurations
Dataset.DatasetName     = 'HynixRoom';
Dataset.VideoExt        = 'avi';
Dataset.ImgExt          = 'jpg';
Dataset.Ext2Imgs        = true;            % turn it on only once to create testing images
%Dataset.LastFrmIdx      = 1500;
Dataset.IsImgEmpty      = ~isempty(dir([Dataset.DatasetName]));

%% Extract images from videos
if(Dataset.IsImgEmpty)
    Dataset.ImgInfo = dir([Dataset.DatasetName,'\*',Dataset.ImgExt]);
end

% Directory preparation
if(~exist( ['./',Dataset.DatasetName],'dir'))
    mkdir( ['./',Dataset.DatasetName]);
end
ImgDirInfo = dir([Dataset.DatasetName,',/*',Dataset.ImgExt]);

Dataset.Ext2Imgs = true;
if(~Dataset.IsImgEmpty)
    fprintf('There exists images in this folder, continue will delete them all.\n');  
    prompt = 'Press Y to delete. Y/N [N]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'N';
    end
    
    if( (str=='Y') || (str=='y'))       
        delete([tmpDir,'\*',Dataset.ImgExt]);
    else
        Dataset.Ext2Imgs = false;
    end
end
    
if(Dataset.Ext2Imgs)
    % Directory preparation
    if(~exist( [Dataset.DatasetName],'dir'))
        mkdir( [Dataset.DatasetName]);
    end
    
    % Extract frame from the input video
    fprintf('Extracting images from the input video ...');
    videoFileReader = vision.VideoFileReader(  [Dataset.DatasetName,'.',...
                                                Dataset.VideoExt]); 
    frameCnt = 0;
    while ~isDone(videoFileReader)
        curFrame = step(videoFileReader);                
        FullFrameName = [Dataset.DatasetName,'\',...
                         sprintf('%0.6d',frameCnt),'.',...
                         Dataset.ImgExt];
        curFrame = imresize(curFrame,[540,960]);
        imwrite(curFrame,FullFrameName);
        frameCnt = frameCnt + 1;
%         figure();
%         imshow(curFrame);              
    end    
    fprintf(' done.\n');
    fprintf('Files are stored at: %s\n',[Dataset.DatasetName,'\']);
end
