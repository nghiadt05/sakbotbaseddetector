%{
    This script creates a horizontally inverse image for a given image
%}
%%
clc; close all; clear all;

%%
imgdir = dir('./*nI.jpg');

%%
for i=1:size(imgdir,1)
    I = imread(imgdir(i).name);
    II = I;
    for j=1:size(I,2)
        Iidx = size(I,2) + 1 - j;        
        II(:,Iidx) = I(:,j);
    end
    imwrite(II,sprintf('./%dI.jpg',i));
%     figure();imshow(I);
%     figure();imshow(II);
end