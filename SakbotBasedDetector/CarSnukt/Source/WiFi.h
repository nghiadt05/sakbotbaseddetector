#ifndef _WIFI_H_
#define _WIFI_H_

#include "Common.h"

#pragma once

#define	AC_ACK			0
#define	AC_CONTROL		1
#define AC_CURPOS		2
#define AC_KP			3
#define AC_KI			4
#define	AC_KD			5
#define AC_KG			6

#define CAR_STOP		0
#define CAR_START		1

#define MyPI			1
#define MyPIFPGA		0

typedef struct {
	unsigned short header;
	union	{
		struct {
			short wvalue;
			short lvalue;
		};
		int		dw;
		float	fv;
	};
} ACMessage;

class CarMSG {
public:
	SOCKET		s;
	ACMessage	m;
	CarMSG();
	~CarMSG();
	int connectToCar(char * ip, unsigned short port);
	int sendFloatData(int header, float data);
	int sendPosData(int header, Point pos);
};

#endif