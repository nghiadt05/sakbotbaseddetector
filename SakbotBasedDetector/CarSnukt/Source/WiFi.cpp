#include "WiFi.h"

CarMSG::CarMSG(){
	int		iResult;
	WSADATA wsaData;

	iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult == NO_ERROR) {
		s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		if (s == INVALID_SOCKET) {
			WSACleanup();
		}
	}
	else {
		s = INVALID_SOCKET;
	}
	m.header = AC_ACK;
}

CarMSG::~CarMSG(){
	if (s != INVALID_SOCKET) {
		closesocket(s);
		WSACleanup();
	}
}

int CarMSG::connectToCar(char * ip, unsigned short port){
	int iResult;
	struct sockaddr_in server;

	server.sin_family = AF_INET;
	InetPton(AF_INET, ip, &server.sin_addr.s_addr);
	server.sin_port = htons(port);

	iResult = connect(s, (SOCKADDR*)&server, sizeof(server));
	if (iResult == SOCKET_ERROR) {
		return -1;
	}
	return 0;
}

int CarMSG::sendFloatData(int header, float data){
	m.header = header;
	m.fv = data;	
	return (SOCKET_ERROR == send(s, (const char*)&m, sizeof(m), 0));
}

int CarMSG::sendPosData(int header, Point pos){
	m.header = header;
	m.wvalue = pos.x;
	m.lvalue = pos.y;
	return (SOCKET_ERROR == send(s, (const char*)&m, sizeof(m), 0));
}