#ifndef _MAIN_H_
#define _MAIN_H_

#include "CarSnukt.h"
#include "WiFi.h"

// Variables, structures and classes
Mat				I, II, III;
uint32_t		FirstImgIdx = 0;
uint32_t		LastImgIdx = 0;
uint32_t		ImgIdx = 0;
Int				BGM_update_cnt = 0;
CarSnukt		myCarSnukt;
CarMSG			car0;

// sending out variables
bool start = false;
double PredDisError = 0.0f;
double CurDisError = 0.0f;
Point2i CurPos(0, 0);
int key;

// Function headers
Void Init();

#if STATIC_IMAGE
inline Mat  ReadImage(int ImgIdx);
#elif VIDEO || CAMERA
inline Mat ReadImage(VideoCapture &cap);
#endif 

#if STATIC_IMAGE
inline Mat ReadImage(int ImgIdx)
#elif VIDEO || CAMERA
inline Mat ReadImage(VideoCapture &cap)
#endif
{
	Mat I;
#if STATIC_IMAGE
	char ImgName[100];
	sprintf(ImgName, "%s%0.6d%s", DATASET_DIR, ImgIdx, FILE_EXT);
	I = imread(ImgName);
#elif VIDEO || CAMERA
	cap >> I;	
#endif		
	assert(I.empty() != 1 && "Cannot read image");
	//printf("\n%d - %d\n", I.rows, I.cols);
	resize(I, I, Size(SIZE_HOR, SIZE_VER));
	I.convertTo(I, CV_32FC3, 1 / 255.0);
	GaussianBlur(I, I, cv::Size(5, 5), 0.3);
	return I;
}

#endif // !_MAIN_H_