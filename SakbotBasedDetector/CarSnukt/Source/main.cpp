﻿#include "main.h"

int main() {

	// assertions 
	assert((VIDEO + CAMERA + STATIC_IMAGE) == 1 && "Only one type of input should be enable at a time");
	assert((ANN + RANTREE + SIZE_SALIENCY) == 1 && "Only one type of classification should be enable at a time");
	assert(BGM_STABLE_CNT > BGM_N);

#if RF_MODULE
	// initialize the serial port	
	BYTE* pBuf = new BYTE[10000];
	memset(pBuf, '%', 10000);
	HANDLE hEvent = NULL;
	int nPortToUse = 6;
	COMMCONFIG config;
	CSerialPort::GetDefaultConfig(nPortToUse, config);
	CSerialPort port;
	port.Open(nPortToUse, 9600, CSerialPort::NoParity, 8, CSerialPort::OneStopBit, CSerialPort::XonXoffFlowControl);
	HANDLE hPort = port.Detach();
	port.Attach(hPort);
	port.Set0WriteTimeout();
	port.Set0ReadTimeout();
#endif 

#if WIFI_MODULE
	// initialize the WIFI connection 
	bool static isConnected[2] = { true, true };
#if MyPI
	if (car0.connectToCar("192.168.0.6", 5678))
	{
		printf("Unable to connect to server 1: %ld\n", WSAGetLastError());
		isConnected[0] = false;
	}
	else
		printf("Connected to car 0: 192.168.0.6\n");
#endif

#if MyPIFPGA
	if (car1.connectToCar("192.168.1.223", 5678))
	{
		printf("Unable to connect to server 2: %ld\n", WSAGetLastError());		
		isConnected[1] = false;
	}
	else
		printf("Connected to car 0: 192.168.1.223\n");
#endif

#endif

	// read the background image 
	myCarSnukt.LoadBG();

#if VIDEO
	// open the video file
	char VideoName[200];
	sprintf(VideoName, "%s", VIDEO_FILE);
	VideoCapture cap(VideoName);
	assert(cap.isOpened() && "Cannot open the video file");
	double count = cap.get(CV_CAP_PROP_FRAME_COUNT);
	assert(LAST_IMG_IDX <= count && "LAST_IMG_IDX is too high");
#elif CAMERA
	VideoCapture cap(0); // open the default camera
	assert(cap.isOpened() && "Cannot open the camera");
#endif

#if VIDEO || CAMERA 
	uint32_t ImgIdx = 0;
	bool isStop = false;
#endif	 
	bool isFirstFrame = true;

#if STATIC_IMAGE
	for (uint32_t tmpImgIdx = FIRST_IMG_IDX; tmpImgIdx < LAST_IMG_IDX; tmpImgIdx = tmpImgIdx + 1)
#elif VIDEO || CAMERA
	// read images from a video file
	while (!isStop)
#endif
	{
#if STATIC_IMAGE
		Mat tmpI = ReadImage(tmpImgIdx);
		ImgIdx = tmpImgIdx - FIRST_IMG_IDX;
#elif VIDEO || CAMERA
		// Read images from the video or a camera			
		Mat tmpI = ReadImage(cap);
#endif
		// Print out the frame number
		//printf("%d\n", ImgIdx);

		// Initilization once
		if (isFirstFrame){
			printf("Define the ROI and the image region for the perspective transformation.\n");
			myCarSnukt.Initialize(tmpI.rows, tmpI.cols);

#if STATIC_ROI
			myCarSnukt.FormROI(ROI_BL, ROI_BR, ROI_TR, ROI_TL);
#if IS_USE_PER_TRANS
			myCarSnukt.FormTransBGM(tmpI, Trans_BL, Trans_BR, Trans_TR, Trans_TL, Trans_W, Trans_H);
#endif
#else
			myCarSnukt.InputROIandPersMap(tmpI);
#endif		
			isFirstFrame = false;
		}

		// Update history images
		if (ImgIdx <= 3){
			III = tmpI;	II = tmpI; I = tmpI;
		}
		else{
			III = II; II = I; I = tmpI;
		}

		// CarSnukt algorithm 
#if BGM_DYNAMIC
		if ((ImgIdx%BGM_DT) == 0)		
			myCarSnukt.UpdateBGM(I);		 
		// CarSnukt Detector 
		myCarSnukt.CarSnuktDet(I, III);
#else 

		// background update if applicable
		if ((ImgIdx%BGM_DT) == 0 && (BGM_update_cnt < BGM_STABLE_CNT) && !myCarSnukt.isBGLoaded())
		{
			myCarSnukt.UpdateBGM(I);
			BGM_update_cnt++;
		}
#if LOAD_STORE_BG
		if (myCarSnukt.GetBGMStatus())
			myCarSnukt.StoreBG();
#endif
		// CarSnukt Detector (main program)		 
		if (myCarSnukt.GetBGMStatus()) // background formation done
		{
			// start/stop and PID tuning
			if (_kbhit()){
				key = _getch();
				if (key == 'q'){
					start = false;
					printf("STOP !\n");
#if WIFI_MODULE
					if (isConnected[0])
						car0.sendFloatData(AC_CONTROL, CAR_STOP);					
#endif
				}
				else if(key == 'e'){
					start = true;
					printf("START !\n");
#if WIFI_MODULE
					if (isConnected[0])
						car0.sendFloatData(AC_CONTROL, CAR_START);
#endif
				}
				else if (key == 'w'){
					start = false;
					printf("PID Tuning !\n");
#if WIFI_MODULE
					if (isConnected[0])
						car0.sendFloatData(AC_CONTROL, CAR_STOP);
#endif
					string sinput = "";
					string sheader = "";
					string svalue = "";
					cin >> sinput;
					for (uint8_t i = 0; i < sinput.size(); i++){
						if (i < 2)
							sheader += sinput[i];
						else if (i>2)
							svalue += sinput[i];
					}
					float fvalue = atof(svalue.c_str());
					if (sheader == "KP"){
#if WIFI_MODULE
						if (isConnected[0])
							car0.sendFloatData(AC_KP, fvalue);
#endif
					}
					else if (sheader == "KD"){
#if WIFI_MODULE
						if (isConnected[0])
							car0.sendFloatData(AC_KD, fvalue);
#endif
					}
					else if (sheader == "KI"){
#if WIFI_MODULE
						if (isConnected[0])
							car0.sendFloatData(AC_KI, fvalue);
#endif
					}
					else if (sheader == "KG"){
#if WIFI_MODULE
						if (isConnected[0])
							car0.sendFloatData(AC_KG, fvalue);
#endif
					}
					cout << "Input = " << sinput << "\n\r";
					cout << "Header = " << sheader << "\n\r";
					cout << "Value = " << fvalue << "\n\r";
				}				
			}

			// the main algorithm
			myCarSnukt.CarSnuktDet(I, III);		
			// Print out the detection results, those data should be transfered to the car using the RF module
			if (countNonZero(myCarSnukt.LiveObjList) > 0 && start){
				vector<Point2i> NonZ;
				findNonZero(myCarSnukt.LiveObjList, NonZ);
				for (size_t i = 0; i < NonZ.size(); i++){
					uint8_t ID = NonZ.at(i).x;
					CurPos = myCarSnukt.TrackObj[ID].CenterImgPlane;					
#if RF_MODULE					 
					sprintf(output, "AT+MSG=255,sDE,%d\n\r", (unsigned int)(DisError + 1000) * 100);
					strOut = output;
					port.Write(strOut.c_str(), strlen(strOut.c_str()));
					port.Flush();
#endif		
					cout << CurPos << "\n";
#if WIFI_MODULE
					if (isConnected[0])
						car0.sendPosData(AC_CURPOS, CurPos);					
#endif
				}
			}
		}
#endif 

		// Check the termination condition		
#if VIDEO
		ImgIdx++;
		if (ImgIdx > LAST_IMG_IDX)
		{
			isStop = true;
		}
#elif CAMERA
		ImgIdx++;
		ImgIdx %= 100000;
#endif
	}

#if RF_MODULE
	port.Close();
#endif		

	return(0);
}

