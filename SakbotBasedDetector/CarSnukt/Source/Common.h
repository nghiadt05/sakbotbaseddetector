#ifndef _COMMON_H_
#define _COMMON_H_

// Define for libararies
//#define _USE_MATH_DEFINES

// Platform
#define WINDOW 1

// C/C++
#include <conio.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>

#if WINDOW
	#include <winsock2.h>
	#include <ws2tcpip.h>
	#include <windows.h>  
	#include <mutex>
	#pragma comment(lib, "ws2_32.lib")
#endif

// OpenCV
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/ml/ml.hpp>
#include <opencv2/video/tracking.hpp>

// Serial port
#include "SerialPort.h" 

using namespace cv;
using namespace std;
using namespace ml;

typedef       void					Void;
typedef       bool					Bool;
typedef       char					TChar;
typedef       signed char			SChar;
typedef       unsigned char			UChar;
typedef       short					Short;
typedef       unsigned short		UShort;
typedef       int					Int;
typedef       unsigned int			UInt;
typedef       double				Double;
typedef       float					Float;

// Type of input (enable for an input only)
#define STATIC_IMAGE				0				  
#define VIDEO						0			  
#define CAMERA						1				  

// Dataset configurations
#define VIDEO_FILE					"../../../Datasets/RC_Car_Vid_1.mp4"
#define DATASET_DIR					"../../../Datasets/"
#define BG_FILE						"../../Bin/BG.jpg"
#define FILE_FORMAT					"%0.6d"
#define FILE_EXT					".jpg"
#define FIRST_IMG_IDX				0 
#define LAST_IMG_IDX				350  

// Image size
#define SIZE_HOR					960
#define SIZE_VER					640 

/*
	ROI
	*/
#define STATIC_ROI					1

/*
	Perspective transformation
	*/
#define IS_USE_PER_TRANS			0

#if STATIC_ROI
///////////////////////////////////////////////////////////////
//////////			ROI static setup			///////////////
///////////////////////////////////////////////////////////////

const Point2i ROI_BL(1, 160);
const Point2i ROI_BR(959, 160);
const Point2i ROI_TR(959, 639);
const Point2i ROI_TL(1, 639);

///////////////////////////////////////////////////////////////
//////////	Perspective transform static setup	///////////////
///////////////////////////////////////////////////////////////

// Test sequence 1 (real serveillance camera)
//const Point2f Trans_BL(298, 58);
//const Point2f Trans_BR(399, 75);
//const Point2f Trans_TR(300, 265);
//const Point2f Trans_TL(2, 150);

#endif
const uint32_t Trans_W = 500;
const uint32_t Trans_H = 450;

/*
	PID target lines
	*/

/*
	CarSnukt detector
	*/

// For background update algorithm
#define BGM_DYNAMIC					0				  // 1: a dynamic background model (BGM) is used, otherwise a statistical BGM is used
#define BGM_WB						1.5				  // The weight for the update of the current background model 
#define BGM_N						3				  // The number of background image candidates
#define BGM_DT						5 				  // The BGM update interval (frames)
#define BGM_KNOWLEDGE				0				  // 1: use the knowledge-base BGM, default 0
#define BGM_STABLE_CNT				BGM_N + 3		

// Flags
#define WIFI_MODULE					1
#define RF_MODULE					0
#define SHADOW_REMOVAL				0
#define TRANSFORM_CRITICAL_POINT	0
#define DETECT_HARD_ID				0
#define DETECT_DIRECTION			0
#define LOAD_STORE_BG				0

// Thresholds and gains
#define TL_MIN						0.1				  // suppression noise level threshold after the background subtraction step (min)
#define TL_MAX						3.00              // suppression noise level threshold after the background subtraction step (max)
#define ALPHA						0.4               // the low threshold for detecting shadow as presented in the paper
#define BETA						1.0               // the high threshold for detecting shadow as presented in the paper
#define SHT							550               // the minimum salient property needed for an object before applying the shadow suppression step
#define OT							30                // the threshold non - zero pixels for an object
#define SAL							50				  // the object should be large enough for MVO and GHOST detection
#define SIZE						10				  // the minimum size of the segmented object
#define LARGE_OBJ_DENS				30				  // the minimum pixel density which indicates a large object
#define LARGE_OBJECT_SIZE			8				  // the minimum size in both horizontal and vertical directions of a large object
#define MVT_MIN						0.002			  // the threshold to differentiate the immediate MV from noise (min)
#define MVT_MAX						3.00			  // the threshold to differentiate the immediate MV from noise (max)
#define BBMIN						6                 // the minimum size of the bounding box
#define SEGMIN						10                // the minimum distance between two separated segments
#define GFTHR_LOW					220				  // the lower bound after applying a Gaussian filter for shadow objects
#define GFTHR_HIGH					255				  // the upper bound after applying a Gaussian filter for shadow objects
#define ROI_VEC						5				  // outer-inner ROI gap
#define VECTOR_UNIT_LENGTH			30
#define MOVING_THRESH				5

// Moving object classification
#define ANN							0
#define RANTREE						0
#define SIZE_SALIENCY				1
#define STORE_MVO					0				 
#define TRAIN_MODEL					0

// Tracking for MVO
#define HIS_POS_SIZE				15
#define LIVE_OBJECT_SIZE			10
#define HISTOGRAM_BIN_SIZE			256
#define UNKNOW_HARD_ID				111
#define DISTANCE_THRES				200
#define SIZE_THRES					200
#define HISTOGRAM_THRES				200
#define PREDICTIVE_TRACK			1
#define KALMAN_TRACK				1

// Target lines
#define ENTRACE_A_RIGHT				1
#define ENTRACE_A_LEFT				2
#define ENTRACE_A_STRAIGHT			3
#define ENTRACE_B_RIGHT				4
#define ENTRACE_B_LEFT				5
#define ENTRACE_B_STRAIGHT			6
#define ENTRACE_C_RIGHT				7
#define ENTRACE_C_LEFT				8
#define ENTRACE_C_STRAIGHT			9
#define ENTRACE_D_RIGHT				10
#define ENTRACE_D_LEFT				11
#define ENTRACE_D_STRAIGHT			12
#define NO_TARGET_LINE_SAMPLE		200

// Debug
#define DEBUG_FINAL					1
#define DEBUG_CRITICAL_POINT		0
#define DEBUG_TRACKING				0
#define DEBUG_NONZ_SEG				0
#define DEBUG_NONZ_SEG_TWICE		0
#define DEBUG_SHADOW_DET			0
#define DEBUG_MVO_CLASSSIFY			0
#define DEBUG_BKG_UPDATE			0
#define DEBUG_TARGET_LINE			0

#endif // !_COMMON_H_
