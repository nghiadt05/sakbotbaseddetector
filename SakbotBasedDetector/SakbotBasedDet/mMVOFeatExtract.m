function [ featVec ] = mMVOFeatExtract( BB, MVO, FetOpt, ImgSizeVer, ImgSizeHor)
%     BB = TrainMVO{i}.BB;
%     MVO = TrainMVO{i}.MVO;
    featVec = [];
    % symmetric fet.
    if FetOpt.UseSymFet
        % verticaly symmetric feature
        if mod(size(MVO,1),2)==1
            tmpMVO = MVO(1:end-1,:);
        else
            tmpMVO = MVO;
        end    
        partVerA = tmpMVO(1:end/2,:);
        partVerB = tmpMVO((end/2+1):end,:);
        SymVer = partVerA - partVerB;
        SymVer = size(find(SymVer),1)/(size(SymVer,1)*size(SymVer,2));
        % horizontaly symmetric feature
        if mod(size(MVO,2),2)==1
            tmpMVO = MVO(:,1:end-1);
        else
            tmpMVO = MVO;
        end
        partHorA = tmpMVO(:,1:end/2);
        partHorB = tmpMVO(:,(end/2+1):end);
        SymHor  = partHorA - partHorB;
        SymHor = size(find(SymHor),1)/(size(SymHor,1)*size(SymHor,2));
        % symmetric feature
        featVec = [featVec;SymVer;SymHor];        
    end
    % size fet.
    if FetOpt.UseSizeFet
        v       = (BB(4)-BB(3))/ImgSizeVer;
        h       = (BB(2)-BB(1))/ImgSizeHor;
        featVec = [featVec;v;h];
    end
    % position fet.
    if FetOpt.UseSizeFet
        pos_v   = (BB(4) + BB(3))/(2*ImgSizeVer);
        pos_h   = (BB(1) + BB(2))/(2*ImgSizeHor);
        featVec = [featVec;pos_v;pos_h];
    end
    % partly density fet
    if FetOpt.UseDenFet        
        density = sum(sum(MVO==1))/(size(MVO,1)*size(MVO,2));
        featVec = [featVec;density];           
    end
end

