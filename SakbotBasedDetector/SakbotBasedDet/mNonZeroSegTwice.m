function [ Segment ] = mNonZeroSegTwice(GrayImg, MinSegDist, IsDisplay)
    tmpSeg = mNonZeroSeg(GrayImg,MinSegDist,false);
    Segment = [];
    if(~isempty(tmpSeg))
        for i=1:size(tmpSeg,1)
            % take the required image region
            HorIdx = [tmpSeg(i,1),tmpSeg(i,2)];
            VerIdx = [tmpSeg(i,3),tmpSeg(i,4)];
            tmpGrayI = GrayImg(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2));
            % re-segment tmpGrayI for higher accuracy bounding boxes
            tmpSegSmall = mNonZeroSeg(tmpGrayI,MinSegDist,false);
            % translate to the big imgage co-ordinate
            for j=1:size(tmpSegSmall)
                tmpNewSeg = [   tmpSeg(i,1)+tmpSegSmall(j,1)-1,...
                                tmpSeg(i,1)+tmpSegSmall(j,2)-1,...   
                                tmpSeg(i,3)+tmpSegSmall(j,3)-1,...   
                                tmpSeg(i,3)+tmpSegSmall(j,4)-1,]; 
                Segment = [Segment;tmpNewSeg];
            end
        end
    end

    if IsDisplay
        figure();
        imshow(GrayImg);
        hold on;
        for i=1:size(Segment,1)
            curSeg = Segment(i,:);
            rect = [curSeg(1)-2,...           % TL_Ver
                    curSeg(3)-2,...           % TL_Hor
                    curSeg(2)-curSeg(1)+4,... % Hor
                    curSeg(4)-curSeg(3)+4];   % Ver
           rectangle('Position',rect,'EdgeColor',[1,1,1]);
        end
    end
end

