%{
    Brief: Extract images from testing Datasetses and store them
%}
global Dataset;

%% Configurations
Dataset.DatasetFolder   = '../Dataset'; 
Dataset.DatasetName     = 'SelfRecord_01';
Dataset.VideoExt        = 'avi';
Dataset.ImgExt          = 'jpg';
Dataset.Ext2Imgs        = false;            % turn it on only once to create testing images
Dataset.LastFrmIdx      = 1500;
Dataset.IsImgExist      = ~isempty(dir([Dataset.DatasetFolder,'\',Dataset.DatasetName]));

%% Extract images from videos
if(Dataset.IsImgExist)
    Dataset.ImgInfo = dir([ [Dataset.DatasetFolder,'\',Dataset.DatasetName],'\*',Dataset.ImgExt]);
end

% Directory preparation
if(~exist( [Dataset.DatasetFolder,'\',Dataset.DatasetName],'dir'))
    mkdir( [Dataset.DatasetFolder,'\',Dataset.DatasetName]);
end
ImgDirInfo = dir([ [Dataset.DatasetFolder,'\',Dataset.DatasetName],'\*',Dataset.ImgExt]);

Dataset.Ext2Imgs = true;
if(Dataset.IsImgExist)
    fprintf('There exists images in this folder, continue will delete them all.\n');  
    prompt = 'Press Y to delete. Y/N [N]: ';
    str = input(prompt,'s');
    if isempty(str)
        str = 'N';
    end
    
    if( (str=='Y') || (str=='y'))       
        delete([tmpDir,'\*',Dataset.ImgExt]);
    else
        Dataset.Ext2Imgs = false;
    end
end
    
if(Dataset.Ext2Imgs)
    % Directory preparation
    if(~exist( [Dataset.DatasetFolder,'\',Dataset.DatasetName],'dir'))
        mkdir( [Dataset.DatasetFolder,'\',Dataset.DatasetName]);
    end
    
    % Extract frame from the input video
    fprintf('Extracting images from the input video ...');
    videoFileReader = vision.VideoFileReader(  [Dataset.DatasetFolder,'\',...
                                                Dataset.DatasetName,'.',...
                                                Dataset.VideoExt]); 
    frameCnt = 0;
    while ~isDone(videoFileReader)
        curFrame = step(videoFileReader);                
        FullFrameName = [Dataset.DatasetFolder,'\',...
                         Dataset.DatasetName,'\',...
                         sprintf('%0.6d',frameCnt),'.',...
                         Dataset.ImgExt];
        imwrite(curFrame,FullFrameName);
        frameCnt = frameCnt + 1;
%         figure();
%         imshow(curFrame);              
    end    
    fprintf(' done.\n');
    fprintf('Files are stored at: %s\n',[Dataset.DatasetFolder,'\',...
                                         Dataset.DatasetName,'\']);
end

%% Basic information of the dataset
Dataset.ImgInfo = dir([ [Dataset.DatasetFolder,'\',Dataset.DatasetName],'\*',Dataset.ImgExt]);
Dataset.SampleImg = imread([Dataset.DatasetFolder,'\',...
                            Dataset.DatasetName,'\',...
                            Dataset.ImgInfo(1).name]);
Dataset.ImgSize = size(Dataset.SampleImg);
Dataset.ImgSizeVer = Dataset.ImgSize(1);
Dataset.ImgSizeHor = Dataset.ImgSize(2);
Dataset.NoOfImg = size(Dataset.ImgInfo,1);
assert(Dataset.LastFrmIdx<=Dataset.NoOfImg, 'We dont have that number of frame :D');
