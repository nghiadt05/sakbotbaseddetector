function mSakbotDet(fIdx)
    global Sakbot;
    global Dataset;
    global BGM;
    
    if(BGM.isBsAva) % start Sakbot iff we have at least the statistic BGM
        %% Initialize the masks
        Sakbot.FG       = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);   % foreground mask
        Sakbot.MVO      = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);   % MVO mask
        Sakbot.GH       = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);   % ghost mask
        Sakbot.MVO_SH   = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);   % MVO-shadow mask
        Sakbot.GH_SH    = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);   % ghost-shadow mask

        % Background suppression
        BGSuppresion();

        % Calculate the shadow mask
        ShadowDetection();           

        % Detect MVO and Ghost areas from the MVOG mask 
        MVOGDetection()

        % Detect the MVO_SH and GH_SH areas using MVO and GH
        ShadowSegmentation();
        
        % Deeper classification including training and testing phases
        MVOSeg = MVOClassification(fIdx);
                
        % Display for debugging
        % mSakbotDebugDisplay(Sakbot.I);

        % Annotation        
        Annotation(fIdx,MVOSeg);
        
    end 

function BGSuppresion()
    global Sakbot;
    global BGM;
    % global Dataset;
    
    I = Sakbot.I;
    Sakbot.DB = abs(I - BGM.B);
    Sakbot.DB = Sakbot.DB(:,:,1).^2 + Sakbot.DB(:,:,2).^2 + Sakbot.DB(:,:,3).^2;
    Sakbot.DB = sqrt(Sakbot.DB);
        
    % eliminate the noises first        
    Sakbot.FG(Sakbot.DB>Sakbot.TL) = 1;     
    
    % gose through the ROI filter mask
    Sakbot.FG = Sakbot.FG .* Sakbot.ROI;
    
    % remove too small objects from the raw foreground, let's treat
    % those small object as noises
    FGSeg = mNonZeroSeg(Sakbot.FG,Sakbot.SEGMIN,false);  % only need to segment once     
    for FGSegIdx = 1:size(FGSeg,1)
        HorIdx = [FGSeg(FGSegIdx,1),FGSeg(FGSegIdx,2)];
        VerIdx = [FGSeg(FGSegIdx,3),FGSeg(FGSegIdx,4)];
        tmpObjt = Sakbot.FG(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2));
        if(sum(sum(tmpObjt)) <= Sakbot.OT)
            % remove the small objects from the current foreground
            Sakbot.FG(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2)) = 0;              
        end
    end   

function ShadowDetection()
    global Sakbot;
    global BGM;
    global Dataset;
    
    I = Sakbot.I;
    Sakbot.SP = zeros(Dataset.ImgSizeVer, Dataset.ImgSizeHor);       
    % only detect shadow for a salient object, once a group of shadow is
    % detected, start the shadow detection refinement process
    % immediately
    FGSeg = mNonZeroSegTwice(Sakbot.FG,Sakbot.SEGMIN,false);
    if(~isempty(FGSeg))
        for FGSegIdx = 1:size(FGSeg,1)
            HorIdx = [FGSeg(FGSegIdx,1),FGSeg(FGSegIdx,2)];
            VerIdx = [FGSeg(FGSegIdx,3),FGSeg(FGSegIdx,4)];
            FGImg = Sakbot.FG(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2));
            nonZeroFGPix = sum(sum(FGImg));
            %fprintf('%d\n',sum(sum(FGImg)));
            if(nonZeroFGPix>=Sakbot.SHT)
                nonZeroSHPix = 0;
                for verIdx = VerIdx(1):VerIdx(2)                        
                    for horIdx = HorIdx(1):HorIdx(2)
                        if(Sakbot.FG(verIdx,horIdx))
                            I_HSV(1:3) = rgb2hsv(I(verIdx,horIdx,:)); 
                            B_HSV(1:3) = rgb2hsv(BGM.B(verIdx,horIdx,:));
                            if( Sakbot.alpha <= I_HSV(3)/B_HSV(3) && ...
                                Sakbot.beta  >= I_HSV(3)/B_HSV(3) )                
                                Sakbot.SP(verIdx,horIdx)=1;
                                nonZeroSHPix = nonZeroSHPix + 1;
                            end
                        end
                    end
                end
                % if there is a wrong shadow detection, redo it
                if(nonZeroSHPix==nonZeroFGPix) 
                    for verIdx = VerIdx(1):VerIdx(2)
                        for horIdx = HorIdx(1):HorIdx(2)
                            if(Sakbot.SP(verIdx,horIdx))
                                Sakbot.SP(verIdx,horIdx) = 0;
                                Sakbot.FG(verIdx,horIdx) = 1;
                            end
                        end
                    end
                end
            end
        end
    end

    % generate the MVOG mask 
    Sakbot.MVOG = Sakbot.FG - Sakbot.SP;   

function MVOGDetection()    
    global Sakbot;
    
    % refine MVO and Ghost areas using the recently raw motion vectors
    Sakbot.MVMask = Sakbot.I - Sakbot.llI;
    Sakbot.MVMask = Sakbot.MVMask(:,:,1).^2 + Sakbot.MVMask(:,:,2).^2 + Sakbot.MVMask(:,:,3).^2;
    Sakbot.MVMask = sqrt(Sakbot.MVMask);
    Sakbot.MVMask(Sakbot.MVMask>Sakbot.MVT)=1;
    Sakbot.MVMask(Sakbot.MVMask<=Sakbot.MVT)=0;
    MOVRefined = Sakbot.MVOG .* Sakbot.MVMask;
    % retake the noiseless points 
    MOVRefinedSeg = mNonZeroSegTwice(MOVRefined,Sakbot.SEGMIN,false);
    for MOVRefinedSegIdx = 1:size(MOVRefinedSeg,1)
        % extract the small image segmentations
        HorIdx = [MOVRefinedSeg(MOVRefinedSegIdx,1),MOVRefinedSeg(MOVRefinedSegIdx,2)];
        VerIdx = [MOVRefinedSeg(MOVRefinedSegIdx,3),MOVRefinedSeg(MOVRefinedSegIdx,4)];
        for verIdx = VerIdx(1):VerIdx(2)
            for horIdx = HorIdx(1):HorIdx(2)
                if(Sakbot.MVOG(verIdx,horIdx))
                    Sakbot.MVO(verIdx,horIdx) = 1;
                end
            end
        end
    end
    Sakbot.GH = Sakbot.MVOG - Sakbot.MVO;

function ShadowSegmentation()
    global Sakbot;
    %global Dadaset;
    %global BGM;
    
    ShadowSeg = mNonZeroSegTwice(Sakbot.SP,Sakbot.SEGMIN,false);
    if(~isempty(ShadowSeg))
        for SHIdx = 1:size(ShadowSeg,1)
            HorIdx = [ShadowSeg(SHIdx,1),ShadowSeg(SHIdx,2)];
            VerIdx = [ShadowSeg(SHIdx,3),ShadowSeg(SHIdx,4)];         
            MVOCoRegion = Sakbot.MVO(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2));
            GHCoRegion = Sakbot.GH(VerIdx(1):VerIdx(2),HorIdx(1):HorIdx(2));
            MVOcnt = sum(sum(MVOCoRegion));
            GHcnt = sum(sum(GHCoRegion));
            if(MVOcnt>GHcnt) % this shadow region is more likely to be connected to the MVO region
                for verIdx = VerIdx(1):VerIdx(2)
                    for horIdx = HorIdx(1):HorIdx(2)
                        if(Sakbot.SP(verIdx,horIdx))
                            Sakbot.MVO_SH(verIdx,horIdx) = 1;
                        end
                    end
                end
            else % this shadow region is more likely to be connected to the Ghost region
                for verIdx = VerIdx(1):VerIdx(2)
                    for horIdx = HorIdx(1):HorIdx(2)
                        if(Sakbot.SP(verIdx,horIdx))
                            Sakbot.GH_SH(verIdx,horIdx) = 1;
                        end
                    end
                end
            end
        end
    end        

    % The known-object is completely updated, turn on the flag
    Sakbot.isKOAva = true;
 
function MVOSeg = MVOClassification(fIdx)
    global Sakbot;    
    global Dataset;
    MVOSeg = [];
    if fIdx < Sakbot.FirstTestFrame  % storing training images
        % store the detected object if require
        if Sakbot.isStoreMVO && mod(fIdx,Sakbot.MVOStoreInterval) == 1
            MVOSeg = mNonZeroSegTwice(Sakbot.MVO,Sakbot.SEGMIN,false);             
            if ~isempty(MVOSeg)                
                for i = 1:size(MVOSeg,1)
                    curSeg = MVOSeg(i,:);
                    if( (curSeg(2)-curSeg(1))>Sakbot.BBMIN || (curSeg(4)-curSeg(3))>Sakbot.BBMIN)
                        tmpMVO = Sakbot.MVO(curSeg(3):curSeg(4), ...
                                            curSeg(1):curSeg(2));
                        Sakbot.TrainMVO{Sakbot.TrainMVOIdx}.BB = curSeg;
                        Sakbot.TrainMVO{Sakbot.TrainMVOIdx}.MVO = tmpMVO;
                        Sakbot.TrainMVOIdx = Sakbot.TrainMVOIdx + 1;
                    end
                end
            end
        end
    else % training and testing
        % store the training MVO to a mat file if applicable
        if Sakbot.isStoreMVO && fIdx == Sakbot.FirstTestFrame
            TrainMVO = Sakbot.TrainMVO;
            save('./Dataset/MVOTrainData/TrainMVO.mat','TrainMVO');
            fprintf('Successfully store MVO to a mat file.\n');
            fprintf('Train the model ... \n');
            MVOModel = mClassifyMVO(Dataset.ImgSizeVer,Dataset.ImgSizeHor);
            save('./Dataset/MVOTrainData/MVOModel.mat','MVOModel');
        end
        % load trained model
        if fIdx == Sakbot.FirstTestFrame
%             load('./Dataset/MVOTrainData/MVOModel.mat','MVOModel');
            fprintf('Re-train the model ... \n');
            MVOModel = mClassifyMVO(Dataset.ImgSizeVer,Dataset.ImgSizeHor);
            Sakbot.MVOModel = MVOModel;
        end
        %% testing
        MVOSeg = mNonZeroSegTwice(Sakbot.MVO,Sakbot.SEGMIN,false);       
        tmpMVOSeg = [];
        % feature extraction
        if ~isempty(MVOSeg)
            MVOSeg(:,end+1) = 1;
            for i = 1:size(MVOSeg,1)
                tmpBB = MVOSeg(i,:);
                if( (tmpBB(2)-tmpBB(1))>Sakbot.BBMIN || (tmpBB(4)-tmpBB(3))>Sakbot.BBMIN)                   
                    tmpMVO  = Sakbot.MVO(MVOSeg(i,3):MVOSeg(i,4), ...
                                         MVOSeg(i,1):MVOSeg(i,2));
                    featVec = mMVOFeatExtract(tmpBB, tmpMVO, Sakbot.FetOpt , Dataset.ImgSizeVer, Dataset.ImgSizeHor); 
                    tmplabel   = Sakbot.MVOModel(featVec);
                    if(tmplabel(1)>=tmplabel(2))
                        tmplabel = 1;
                    else
                        tmplabel = 0;
                    end
                    MVOSeg(i,end) = tmplabel;
                    tmpMVOSeg = [tmpMVOSeg;MVOSeg(i,:)];
                end
            end
            MVOSeg = tmpMVOSeg; 
            imshow(MVOSeg);
            MVOSeg= MVOSeg;
        end        
    end
    
function Annotation(fIdx,MVOSeg)
    global Sakbot;
    global Dataset;
    if Sakbot.isAnnotate        
        if(fIdx==1)           
            figure();        
            hold on;
        end
        clf;
        imshow(Sakbot.I);
        if fIdx < Sakbot.FirstTestFrame
            MVOSeg = mNonZeroSegTwice(Sakbot.MVO,Sakbot.SEGMIN,false);            
            if ~isempty(MVOSeg)
                MVOSeg(:,end+1) = 1;
            end
        end
        if ~isempty(MVOSeg)            
            for tmpIdx=1:size(MVOSeg,1)
                curSeg = MVOSeg(tmpIdx,:);
                if( (curSeg(2)-curSeg(1))>Sakbot.BBMIN || (curSeg(4)-curSeg(3))>Sakbot.BBMIN)
                    rect = [curSeg(1)-2,...           % TL_Ver
                            curSeg(3)-2,...           % TL_Hor
                            curSeg(2)-curSeg(1)+4,... % Hor
                            curSeg(4)-curSeg(3)+4];   % Ver
                    if(MVOSeg(tmpIdx,end)==1)
                        color = [1,0,0];                            
                    else
                        color = [0,1,0];                            
                    end
                    % bounding box
                    rectangle('Position',rect,'EdgeColor',color,'LineWidth',1.5);                                            
                end
            end
        end
        % frame number 
        if fIdx < Sakbot.FirstTestFrame
            txt = 'No MVO classification';
        else
            txt = 'With MVO classification';
        end
        text(   130,10,sprintf('Frame: %d/%d - %s',fIdx, Dataset.LastFrmIdx,txt),'color',[0 1 0], ...
                'HorizontalAlignment','center', ...
                'FontSize',10, 'FontUnits','pixels','FontWeight','bold');
        pause(0.01);          
        % write to video file
        if Sakbot.isSave2Video 
            im2save = getframe(gcf);
            im2save = im2save.cdata;
            writeVideo(Sakbot.v,im2save);
        end
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
        
    