%{
    Sakbot stands for Statistical and Knowledge-Based ObjecT detection,
    which is introduced in the PAMI 2003 paper (~ 1500 citation) below:
    =======================================================================
       Cucchiara, Rita, et al. "Detecting moving objects, ghosts, and
       shadows in video streams." Pattern Analysis and Machine
       Intelligence, IEEE Transactions on 25.10 (2003): 1337-1342 
    =======================================================================

    This program is the implementation of the Sakbot detector with the aims
    of distinguishing large-scale and small-scale moving objects for static
    surveillance cameras.

    Seoul, May/09/2016
    Author:    Doan Trung Nghia, CAPP, SNU 
%}

%%
clc; close all; clear all;

%% global variables
global Dataset;
global Sakbot;

%% Datasetset configuration
mDatasetConfig;

%% Initialization
mSakbotConfig;

%% The main algorithm
Dataset.LastFrmIdx = Dataset.NoOfImg - 100;
for fIdx = 1:Dataset.LastFrmIdx        
    % update the background model
    mBGM_Update(fIdx);
    
    % The main Sakbot detector        
    mSakbotDet(fIdx);      
end

% close the video object
if Sakbot.isAnnotate && Sakbot.isSave2Video
    close(Sakbot.v);
end

