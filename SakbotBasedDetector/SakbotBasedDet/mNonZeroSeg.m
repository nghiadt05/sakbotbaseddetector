%{
    Segment nonzero parts of a gray image to smaller parts
    Input:  GrayImg: an input gray image
            MinSegDist: the smallest distance between each object (8 is a
            good setup)
            IsDisplay: display the segmentaions if it's true
    
%}
function Segment = mNonZeroSeg(GrayImg,MinSegDist,IsDisplay)
    % %%
    % clc; close all; clear all;
    % 
    % %%
    % global Sakbot;
    % load('tmpSakbot.mat');
    % % figure();
    % % imshow(Sakbot.FG);
    % figure();
    % imshow(Sakbot.SP);
    % % figure();
    % % imshow(Sakbot.MVOG);
    % %% Config for getting the bounding box
    % MinSegDist = 8; % pixel
    % Sakbot.SearchStep = 5; % pixel
    % GrayImg = Sakbot.FG;

    %%
    MaxVer = size(GrayImg,1);
    MaxHor = size(GrayImg,2);
    
    %% Create the horizontal segmentation
    HorSeg = [];
    Segment = []; 
    [~,indHor] = find(GrayImg==1);
    if isempty(indHor)
        return
    end
    uniqHor = unique(indHor);
    HorSegIdx = 1;
    HorSeg(HorSegIdx,1) = uniqHor(1);
    if size(uniqHor,1)>1
        for uniHorIdx = 1:(size(uniqHor,1)-1)
            curHor = uniqHor(uniHorIdx);
            nxtHor = uniqHor(uniHorIdx+1);
            if( (nxtHor-curHor) >= MinSegDist) 
                % take the ending position of the current seg
                HorSeg(HorSegIdx,2) = min(curHor,MaxHor);
                % move to the next segmentation
                HorSegIdx = HorSegIdx + 1; 
                % take the starting position of the new seg
                HorSeg(HorSegIdx,1) = min(nxtHor,MaxHor);
            end
            if uniHorIdx == (size(uniqHor,1)-1)
                HorSeg(HorSegIdx,2) = min(nxtHor,MaxHor);
            end
        end
    else
        HorSeg(HorSegIdx,2) = min(uniqHor(1),MaxHor);
    end

    %% Create the vertical segmentaion from the horizontal segmentation
    for i=1:size(HorSeg,1)
        % split the current vertical segmentaion from the foreground
        tmpVerSeg = GrayImg(:,HorSeg(i,1):HorSeg(i,2));   
        % find the unique vertical indices
        [indVer,~] = find(tmpVerSeg==1);
        uniqVer = unique(indVer);
        % construct the vertical segmentations from the current horizontal
        % segmentation
        VerSegIdx = 1;
        VerSeg(VerSegIdx,1) = uniqVer(1);
        if size(uniqVer,1)>1
            for uniVerIdx = 1:(size(uniqVer,1)-1)
                curVer = uniqVer(uniVerIdx);
                nxtVer = uniqVer(uniVerIdx+1);
                if( (nxtVer-curVer) >= MinSegDist) 
                    % take the ending position of the current seg
                    VerSeg(VerSegIdx,2) = min(curVer,MaxVer);
                    % move to the next segmentation
                    VerSegIdx = VerSegIdx + 1; 
                    % take the starting position of the new seg
                    VerSeg(VerSegIdx,1) = min(nxtVer,MaxVer);
                end
                if uniVerIdx == (size(uniqVer,1)-1)
                    VerSeg(VerSegIdx,2) = min(nxtVer,MaxVer);
                end
            end
        else
            VerSeg(VerSegIdx,2) = min(uniqVer(1),MaxVer);
        end

        for j=1:VerSegIdx
            Segment = [Segment;[HorSeg(i,:),VerSeg(j,:)]];
        end
    end

    %% Display the generated segmentaions
    if(IsDisplay)
        figure();
        imshow(GrayImg);
        hold on;
        for i=1:size(Segment,1)
            curSeg = Segment(i,:);
            rect = [curSeg(1)-2,...           % TL_Ver
                    curSeg(3)-2,...           % TL_Hor
                    curSeg(2)-curSeg(1)+4,... % Hor
                    curSeg(4)-curSeg(3)+4];   % Ver
           rectangle('Position',rect,'EdgeColor',[1,1,1]);
        end
    end
    
end

