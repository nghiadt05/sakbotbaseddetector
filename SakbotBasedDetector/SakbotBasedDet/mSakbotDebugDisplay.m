function mSakbotDebugDisplay(I)
    global Sakbot;
    global BGM;
    close all;
    figure('name','Foreground');        imshow(Sakbot.FG);    
    figure('name','Shadow mask');       imshow(Sakbot.SP);
    figure('name','MVOG mask');         imshow(Sakbot.MVOG);
    figure('name','Raw MV mask');       imshow(Sakbot.MVMask);
    figure('name','MVO');               imshow(Sakbot.MVO);   
    figure('name','Ghost');             imshow(Sakbot.GH);
    figure('name','MVO shadow');        imshow(Sakbot.MVO_SH);
    figure('name','Ghost shadow');      imshow(Sakbot.GH_SH);
    figure('name','Current BGM');       imshow(BGM.B);
    figure('name','Current image');     imshow(I);
    MVOSeg = mNonZeroSegTwice(Sakbot.MVO,8,false);
          
    figure('name','Annotation');        
    hold on;
    imshow(I);
    for i=1:size(MVOSeg,1)
        curSeg = MVOSeg(i,:);
        if( (curSeg(2)-curSeg(1))>Sakbot.BBMIN || (curSeg(4)-curSeg(3))>Sakbot.BBMIN)
        rect = [curSeg(1)-2,...           % TL_Ver
                curSeg(3)-2,...           % TL_Hor
                curSeg(2)-curSeg(1)+4,... % Hor
                curSeg(4)-curSeg(3)+4];   % Ver
        rectangle('Position',rect,'EdgeColor',[1,0,0]);
        end
    end
    hold off;
    
end

