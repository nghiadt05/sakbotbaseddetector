%{
    All configurations for the Sakbot implementation
%}

%% Dataset
global Dataset;

%% Background model
global BGM;
BGM.wb                  = 1.5;              % The weighted value accompanies with the current background model used in the statistical set of images
BGM.n                   = 11;               % The size of the statistical image set, including the weighted current background model
BGM.dt                  = 20;               % The sample interval of the statistical image set
for i=1:BGM.n 
    BGM.S{i}            = {};               % The statistical image set, which is used to form the statistical background model
end
BGM.Bs                  = [];               % The statistical background model
BGM.B                   = [];               % The final background model
BGM.isBsAva             = false;            % The flag that indicates the statistical background model (BGM) is formed or not
BGM.isBAva              = false;            % The flag that indicates the final background model (BGM) is formed or not
BGM.DisType1            = 'L-inf';          % 
BGM.DisType2            = 'Euler';          % 
BGM.Distance            = BGM.DisType2;     % Using the Euler distance for updating BGM requires less computing time as compared to the L-inf distance (pixel-wise) 

%% Sakbot detector
global Sakbot;
% labels
Sakbot.LABEL_UKN        = 1;                % unknown objects 
Sakbot.LABEL_MVO        = 2;                % real moving visual objects
Sakbot.LABEL_GH         = 3;                % ghost 
Sakbot.Label_MVO_SH     = 4;                % shadow object of MVO
Sakbot.Label_GH_SH      = 5;                % shadow object of ghost
% thresholds
Sakbot.TL               = 0.25;             % suppression noise level threshold after the background subtraction step  
Sakbot.alpha            = 0.4;              % the low threshold for detecting shadow as presented in the paper
Sakbot.beta             = 0.9;              % the high threshold for detecting shadow as presented in the paper
Sakbot.tS               = 0.5;              % the threshold set for Saturation color channel in the shadow detection step (not effective)
Sakbot.tH               = 1.5;              % the threshold set for Hue color channel in the shadow detection step (not that effective)
Sakbot.SHT              = 200;              % the minimum salient property needed for an object before applying the shadow suppression step
Sakbot.OT               = 30;               % the threshold non-zero pixels for an object
Sakbot.GHT              = 1e-5;             % the optical flow threshold to detect ghost 
Sakbot.SAL              = 30;               % the object should be large enough for MVO and GHOST detection
Sakbot.MVT              = 0.04;             % the threshold to differentiate the immediate MV from noise
Sakbot.BBMIN            = 10;               % the minimum size of the bounding box
Sakbot.SEGMIN           = 8;                % the minimum distance between two separated segments
% flags
Sakbot.isKOAva          = false;            % the flag which indicates all objects are sucessfully derived from the current foreground
Sakbot.isAnnotate       = true;             % turn on for visuallisation
Sakbot.isSave2Video     = true;             % turn on to save the results into a video

% video settings
if Sakbot.isAnnotate && Sakbot.isSave2Video        
    if (~exist('./Results','dir'))
        mkdir('./Results');
    end
    Sakbot.v = VideoWriter('./Results/SakbotDemo.avi');  
    Sakbot.v.FrameRate = 30;
    Sakbot.v.Quality = 100;
    open(Sakbot.v);
end

% Classification configurations for detected moving objects
Sakbot.ROI                  = [ 1, ...                  % [L T BT R], will be given by another algorithm 
                                60, ...
                                Dataset.ImgSizeVer, ...
                                Dataset.ImgSizeHor]; 
ROItmp                      = zeros(Dataset.ImgSizeVer, Dataset.ImgSizeHor);
ROItmp(Sakbot.ROI(2):Sakbot.ROI(3),Sakbot.ROI(1):Sakbot.ROI(4)) = 1;
Sakbot.ROI                  = ROItmp;                 
Sakbot.FirstTestFrame       = 600;                      % If frmIDx < Sakbot.FirstTestFrame: training, otherwise testing
assert(Sakbot.FirstTestFrame<=Dataset.LastFrmIdx);       
Sakbot.isStoreMVO           = false;                    % Store the MVO for MVO training, require once
Sakbot.MVOStoreInterval     = 2;                        % The MVO storing interval between frames
Sakbot.TrainMVOIdx          = 1;                        % The MVO indx for training
% feture types
Sakbot.FetOpt.UseSymFet     = true;                     % True: use symmetric features (horizontal and vertical features)
Sakbot.FetOpt.UseDenFet     = true;                     % True: use the density features (can use partial density feature)
Sakbot.FetOpt.UseSizeFet    = true;                     % True: use the MVO size as a feature
Sakbot.FetOpt.UsePosFet     = true;                    % True: use the average position of the MVO as a feature, scene-based feature, turn it off for better generalization

if ~exist('./Dataset/MVOTrainData/')
    mkdir('./Dataset/MVOTrainData/');
end




