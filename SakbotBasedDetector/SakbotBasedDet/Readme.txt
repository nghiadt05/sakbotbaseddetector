=======================================================================
=======================================================================

Sakbot stands for Statistical and Knowledge-Based ObjecT detection,
which is introduced in the PAMI 2003 paper (~ 1500 citation) below:
=======================================================================
   Cucchiara, Rita, et al. "Detecting moving objects, ghosts, and
   shadows in video streams." Pattern Analysis and Machine
   Intelligence, IEEE Transactions on 25.10 (2003): 1337-1342 
=======================================================================

This program is the implementation of the Sakbot detector with the aims
of distinguishing large-scale and small-scale moving objects for static
surveillance cameras.

Additional features:
- Using the detected MVO results to further classify those moving objects,
those are large-scale MVO (often cars) and the small-scale MVO (pedestrians)

Seoul, May/09/2016
Author:    Doan Trung Nghia, CAPP, SNU 
=======================================================================
=======================================================================

=======================================================================
=======================================================================
To run the program please:

1. There is currently a built-in testing video sequence for the Sakbot program. 
Which means you can modify the program without concerning other modifications.
If you want to test new datasets, please change the following code lines:

Dataset.DatasetFolder   = './Dataset';   % Link to your dataset
Dataset.DatasetName     = 'SelfRecord_01';								  % Your video name	
Dataset.VideoExt        = 'avi';										  % The video extension
Dataset.ImgExt          = 'jpg';										  % The image extension
Dataset.LastFrmIdx      = 600;											  % The number of frames that you want to process

2. Type: "mSakbot" or run the file mSakbot.m in Matlab 
=======================================================================
=======================================================================
