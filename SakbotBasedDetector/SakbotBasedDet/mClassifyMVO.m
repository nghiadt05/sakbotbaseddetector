%{
    After obtaining MVO from Sakbot detector, this program using a NN-based
    learning method for categorizing large-scale moving objects and
    small-scale moving objects.
%}
function net = mClassifyMVO(ImgSizeVer,ImgSizeHor)

global Sakbot;
%%
% clc; close all; clear all;
% ImgSizeVer = 273;
% ImgSizeHor = 480;
% Sakbot.FetOpt.UseSymFet     = true;
% Sakbot.FetOpt.UseDenFet     = true;
% Sakbot.FetOpt.UseSizeFet    = true;
% Sakbot.FetOpt.UsePosFet     = false;
%%
load('../Dataset/MVOTrainData/TrainMVO.mat');

%% hand-labeling 
if ~exist('./Dataset/MVOTrainData/Label.mat','file')
    figure();
    hold on;
    Label = [];
    i = 1;
    while (i <= size(TrainMVO,2))
        clf;
        imshow(TrainMVO{i}.MVO);
        fprintf('%d/%d \n',i,size(TrainMVO,2));
        prompt = 'L: large-scale , S: small-scale , B: back. L/S/B [S]: ';
        str = input(prompt,'s');
        if isempty(str)
            str = 'S';
        end

        if( (str=='L') || (str=='l'))    
            Label(i) = 1;
            i = i+1;
        elseif( (str=='B') || (str=='b'))
            i = i - 1;
            if(i==0)
                i = 1;
            end
        else
            Label(i) = 0;
            i = i + 1;
        end
    end
    save('./Dataset/MVOTrainData/Label.mat','Label');  
else
    load('./Dataset/MVOTrainData/Label.mat');
end

%% training
% feature extraction
X = [];
Y = [];
for i=1:size(TrainMVO,2)
    tmpBB = TrainMVO{i}.BB;
    tmpMVO = TrainMVO{i}.MVO;
    featVec = mMVOFeatExtract(tmpBB, tmpMVO, Sakbot.FetOpt, ImgSizeVer, ImgSizeHor);
    X = [X,featVec];
    if(Label(i)==1)
        Y = [Y,[1;0]];
    else
        Y = [Y,[0;1]];
    end
end

%% using NN to train
net = fitnet([6,6]);
[net,tr] = train(net,X,Y);
nntraintool
plotperform(tr)
testX = X(:,tr.testInd);
testT = Y(:,tr.testInd);
testY = net(testX);
testIndices = vec2ind(testY);
plotconfusion(testT,testY);
