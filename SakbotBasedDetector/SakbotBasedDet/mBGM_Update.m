function mBGM_Update(fIdx)
    % global variables
    global BGM;
    global Sakbot;
    global Dataset
    
    %% read the current image from file
    % print out the progress
    fprintf('frame %d\n',fIdx);
    I = im2double(imread([  Dataset.DatasetFolder,'\',...
                            Dataset.DatasetName,'\',...
                            Dataset.ImgInfo(fIdx).name]));
    I = imgaussfilt(I,0.5); 
    
    if(fIdx==1)        
        Sakbot.llI =  I;
        Sakbot.lI = I;
        Sakbot.I = I;
    else
        Sakbot.llI =  Sakbot.lI;
        Sakbot.lI = Sakbot.I;
        Sakbot.I = I;
    end
    % for some first images, when the background model hasn't yet formed, 
    % hence, we use the first image as the background model
    if(fIdx==1); BGM.B = I; end 
    
    %% Update the statistical background model    
    mBGM_UpdateS_Bs(fIdx,I);
    
%     % Update the knowledge-based background model
%     if(~isempty(BGM.Bs))
%         BGM.Bk = BGM.Bs; 
%     end;
    
    % Update the final bacground model
    if(BGM.isBsAva)
        if (~Sakbot.isKOAva)
            BGM.B = BGM.Bs;
        else % update the background model completely
            for verIdx = 1:Dataset.ImgSizeVer
                for horIdx = 1:Dataset.ImgSizeHor
                    isMVOorMVOGH =  Sakbot.MVO(verIdx,horIdx)|...
                                    Sakbot.MVO_SH(verIdx,horIdx);
                    if(~isMVOorMVOGH)    
                        BGM.B(verIdx,horIdx,:) = BGM.Bs(verIdx,horIdx,:);
                    end
                end
            end
        end
    end

function mBGM_UpdateS_Bs(fIdx,I)
    global BGM;
    if(mod(fIdx,BGM.dt)==1)
        % fill the statistical image set S
        if(isempty(BGM.S{BGM.n-1})) 
            for i=1:(BGM.n-1)
                if(isempty(BGM.S{i}))
                    BGM.S{i} = I;
                    break;
                end
            end
        % update the oldest element by the new image
        else
            for i=1:(BGM.n-2)
                BGM.S{i} = BGM.S{i+1};
            end
            BGM.S{BGM.n-1} = I; 
            BGM.S{BGM.n} = BGM.wb.*BGM.B;         
            % update the current statistic background model 
%             tic;
            mBGM_UpdateBs;         
            BGM.isBsAva = true;
%             toc;
%             close all;
%             figure();imshow(I);
%             figure();imshow(BGM.Bs);
        end
    end


function mBGM_UpdateBs
    global Dataset;
    global BGM;
    if BGM.Distance == BGM.DisType1
        for ver = 1:Dataset.ImgSizeVer
            for hor = 1:Dataset.ImgSizeHor   
                % calculate the L-inf distance of the RBG space for
                % each pixel in the S set
                D_xi_allxj = [];                    
                for i=1:BGM.n                        
                    xi = BGM.S{i}(ver,hor,:);
                    tmpD_xi_allxj = 0;
                    for j=1:BGM.n
                        xj = BGM.S{j}(ver,hor,:);
                        % calculate the L-inf distance between pi and pj
                        D_xi_xj = 0;
                        D_xiR_pjR = abs(xi(1) - xj(1)); D_xi_xj = max(D_xi_xj, D_xiR_pjR);                          
                        D_xiR_pjG = abs(xi(1) - xj(2)); D_xi_xj = max(D_xi_xj, D_xiR_pjG);
                        D_xiR_pjB = abs(xi(1) - xj(3)); D_xi_xj = max(D_xi_xj, D_xiR_pjB);

                        D_xiG_pjR = abs(xi(2) - xj(1)); D_xi_xj = max(D_xi_xj, D_xiG_pjR);
                        D_xiG_pjG = abs(xi(2) - xj(2)); D_xi_xj = max(D_xi_xj, D_xiG_pjG);
                        D_xiG_pjB = abs(xi(2) - xj(3)); D_xi_xj = max(D_xi_xj, D_xiG_pjB);

                        D_xiB_pjR = abs(xi(3) - xj(1)); D_xi_xj = max(D_xi_xj, D_xiB_pjR);
                        D_xiB_pjG = abs(xi(3) - xj(2)); D_xi_xj = max(D_xi_xj, D_xiB_pjG);
                        D_xiB_pjB = abs(xi(3) - xj(3)); D_xi_xj = max(D_xi_xj, D_xiB_pjB);

                        tmpD_xi_allxj =  tmpD_xi_allxj + D_xi_xj;
                    end
                    D_xi_allxj(i) = tmpD_xi_allxj;
                end
                % update the values for the current pixel of Bs
                min_i_idx = find(D_xi_allxj==min(D_xi_allxj));
                min_i_idx = min_i_idx(1); 
                BGM.Bs(ver,hor,:) = BGM.S{min_i_idx}(ver,hor,:);
            end
        end
    elseif BGM.Distance == BGM.DisType2
        % calculate the distance from Si to all Sj
        for i=1:BGM.n
            tmp_D_i_allj = zeros(Dataset.ImgSizeVer,Dataset.ImgSizeHor);
            for j=1:BGM.n
                tmp_D_i_j = BGM.S{i}-BGM.S{j};
                tmp_D_i_j = tmp_D_i_j(:,:,1).^2 + ...
                            tmp_D_i_j(:,:,2).^2 + ...
                            tmp_D_i_j(:,:,3).^2;
                tmp_D_i_j = sqrt(tmp_D_i_j);
                tmp_D_i_allj = tmp_D_i_allj + tmp_D_i_j;
            end
            D_i_allj{i} = tmp_D_i_allj;            
        end
        % pick the pixel in Si that has the smallest distance to other
        % corresponding pixels in all other Sj
        for ver = 1:Dataset.ImgSizeVer
            for hor = 1:Dataset.ImgSizeHor  
                DistArray = [];
                for i=1:BGM.n
                    DistArray = [DistArray,D_i_allj{i}(ver,hor)];
                end
                idx = find(DistArray == min(DistArray));
                idx = idx(1);
                BGM.Bs(ver,hor,:) = BGM.S{idx}(ver,hor,:);
            end
        end
    end
   