The main algorithm performs the following tasks: 
1. Generate a background image from the video sequence 
2. Detect moving objects using a background-substraction-based algorithm
3. Remove shadow/ghost pixels and segment all salient moving objects
4. Match and track moving objects using color histogram and Kalman filter
5. Send the global positions of the moving objects to those objects, this information is used for the control algorithm that detects conflict and grants piority for multiple moving objects

As I have left the project, the final demo that I conducted only concerns with 1 car. The position information is used to control this car moving arround a certain route repeatedly, where the simple PID algorithm is made use of.

The route shown below is composed by multiple target lines, each of them has a set of specific PID gains. The car moves from a target line to another step by step. When it enters a new target line, the corresponding PID gains of that target line are loaded.

![[Link Text](Link URL)](https://bitbucket.org/repo/g8KMxx/images/901244432-Route0.jpg)

[Demo](https://drive.google.com/file/d/0BxLegIn7aAoEYVpiU1ZzZlJ5dUU/view)